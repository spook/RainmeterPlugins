#include "QueryTokenizer.h"

const int IsAcceptState[32] = {-1, -1, 2, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1};

const int TargetState[32] = {-1, -1, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 0, 0, 0};

QueryToken::QueryToken() : 
	tokenType(ttUnknown),
	TokenStr(L"")
{

}

int QueryTokenizer::Transition(int state, const WCHAR ch)
{
	if (state == 0)
	{
		if (ch == 40)
		{
			return 1;
		}
		else if (ch == 46)
		{
			return 2;
		}
		else if (ch >= 65 && ch < 91)
		{
			return 3;
		}
		else if (ch == 91)
		{
			return 4;
		}
		else if (ch == 95)
		{
			return 3;
		}
		else if (ch >= 97 && ch < 123)
		{
			return 3;
		}
	}
	else if (state == 1)
	{
		if (ch == 34)
		{
			return 5;
		}
		else if (ch == 39)
		{
			return 6;
		}
		else if (ch >= 48 && ch < 58)
		{
			return 7;
		}
	}
	else if (state == 3)
	{
		if (ch >= 48 && ch < 58)
		{
			return 3;
		}
		else if (ch >= 65 && ch < 91)
		{
			return 3;
		}
		else if (ch == 95)
		{
			return 3;
		}
		else if (ch >= 97 && ch < 123)
		{
			return 3;
		}
	}
	else if (state == 4)
	{
		if (ch == 34)
		{
			return 8;
		}
		else if (ch == 39)
		{
			return 9;
		}
		else if (ch >= 48 && ch < 58)
		{
			return 10;
		}
	}
	else if (state == 5)
	{
		if (ch >= 0 && ch < 92)
		{
			return 11;
		}
		else if (ch == 92)
		{
			return 12;
		}
		else if (ch >= 93)
		{
			return 11;
		}
	}
	else if (state == 6)
	{
		if (ch >= 0 && ch < 92)
		{
			return 13;
		}
		else if (ch == 92)
		{
			return 14;
		}
		else if (ch >= 93)
		{
			return 13;
		}
	}
	else if (state == 7)
	{
		if (ch == 41)
		{
			return 15;
		}
		else if (ch >= 48 && ch < 58)
		{
			return 7;
		}
	}
	else if (state == 8)
	{
		if (ch >= 0 && ch < 92)
		{
			return 16;
		}
		else if (ch == 92)
		{
			return 17;
		}
		else if (ch >= 93)
		{
			return 16;
		}
	}
	else if (state == 9)
	{
		if (ch >= 0 && ch < 92)
		{
			return 18;
		}
		else if (ch == 92)
		{
			return 19;
		}
		else if (ch >= 93)
		{
			return 18;
		}
	}
	else if (state == 10)
	{
		if (ch >= 48 && ch < 58)
		{
			return 10;
		}
		else if (ch == 93)
		{
			return 15;
		}
	}
	else if (state == 11)
	{
		if (ch >= 0 && ch < 34)
		{
			return 11;
		}
		else if (ch == 34)
		{
			return 20;
		}
		else if (ch >= 35 && ch < 92)
		{
			return 11;
		}
		else if (ch == 92)
		{
			return 21;
		}
		else if (ch >= 93)
		{
			return 11;
		}
	}
	else if (state == 12)
	{
		if (ch >= 0)
		{
			return 11;
		}
	}
	else if (state == 13)
	{
		if (ch >= 0 && ch < 39)
		{
			return 13;
		}
		else if (ch == 39)
		{
			return 22;
		}
		else if (ch >= 40 && ch < 92)
		{
			return 13;
		}
		else if (ch == 92)
		{
			return 23;
		}
		else if (ch >= 93)
		{
			return 13;
		}
	}
	else if (state == 14)
	{
		if (ch >= 0)
		{
			return 13;
		}
	}
	else if (state == 16)
	{
		if (ch >= 0 && ch < 34)
		{
			return 16;
		}
		else if (ch == 34)
		{
			return 24;
		}
		else if (ch >= 35 && ch < 92)
		{
			return 16;
		}
		else if (ch == 92)
		{
			return 25;
		}
		else if (ch >= 93)
		{
			return 16;
		}
	}
	else if (state == 17)
	{
		if (ch >= 0)
		{
			return 16;
		}
	}
	else if (state == 18)
	{
		if (ch >= 0 && ch < 39)
		{
			return 18;
		}
		else if (ch == 39)
		{
			return 26;
		}
		else if (ch >= 40 && ch < 92)
		{
			return 18;
		}
		else if (ch == 92)
		{
			return 27;
		}
		else if (ch >= 93)
		{
			return 18;
		}
	}
	else if (state == 19)
	{
		if (ch >= 0)
		{
			return 18;
		}
	}
	else if (state == 20)
	{
		if (ch >= 0 && ch < 34)
		{
			return 11;
		}
		else if (ch == 34)
		{
			return 20;
		}
		else if (ch >= 35 && ch < 41)
		{
			return 11;
		}
		else if (ch == 41)
		{
			return 28;
		}
		else if (ch >= 42 && ch < 92)
		{
			return 11;
		}
		else if (ch == 92)
		{
			return 21;
		}
		else if (ch >= 93)
		{
			return 11;
		}
	}
	else if (state == 21)
	{
		if (ch >= 0)
		{
			return 11;
		}
	}
	else if (state == 22)
	{
		if (ch >= 0 && ch < 39)
		{
			return 13;
		}
		else if (ch == 39)
		{
			return 22;
		}
		else if (ch == 40)
		{
			return 13;
		}
		else if (ch == 41)
		{
			return 29;
		}
		else if (ch >= 42 && ch < 92)
		{
			return 13;
		}
		else if (ch == 92)
		{
			return 23;
		}
		else if (ch >= 93)
		{
			return 13;
		}
	}
	else if (state == 23)
	{
		if (ch >= 0)
		{
			return 13;
		}
	}
	else if (state == 24)
	{
		if (ch >= 0 && ch < 34)
		{
			return 16;
		}
		else if (ch == 34)
		{
			return 24;
		}
		else if (ch >= 35 && ch < 92)
		{
			return 16;
		}
		else if (ch == 92)
		{
			return 25;
		}
		else if (ch == 93)
		{
			return 30;
		}
		else if (ch >= 94)
		{
			return 16;
		}
	}
	else if (state == 25)
	{
		if (ch >= 0)
		{
			return 16;
		}
	}
	else if (state == 26)
	{
		if (ch >= 0 && ch < 39)
		{
			return 18;
		}
		else if (ch == 39)
		{
			return 26;
		}
		else if (ch >= 40 && ch < 92)
		{
			return 18;
		}
		else if (ch == 92)
		{
			return 27;
		}
		else if (ch == 93)
		{
			return 31;
		}
		else if (ch >= 94)
		{
			return 18;
		}
	}
	else if (state == 27)
	{
		if (ch >= 0)
		{
			return 18;
		}
	}
	else if (state == 28)
	{
		if (ch >= 0 && ch < 34)
		{
			return 11;
		}
		else if (ch == 34)
		{
			return 20;
		}
		else if (ch >= 35 && ch < 92)
		{
			return 11;
		}
		else if (ch == 92)
		{
			return 21;
		}
		else if (ch >= 93)
		{
			return 11;
		}
	}
	else if (state == 29)
	{
		if (ch >= 0 && ch < 39)
		{
			return 13;
		}
		else if (ch == 39)
		{
			return 22;
		}
		else if (ch >= 40 && ch < 92)
		{
			return 13;
		}
		else if (ch == 92)
		{
			return 23;
		}
		else if (ch >= 93)
		{
			return 13;
		}
	}
	else if (state == 30)
	{
		if (ch >= 0 && ch < 34)
		{
			return 16;
		}
		else if (ch == 34)
		{
			return 24;
		}
		else if (ch >= 35 && ch < 92)
		{
			return 16;
		}
		else if (ch == 92)
		{
			return 25;
		}
		else if (ch >= 93)
		{
			return 16;
		}
	}
	else if (state == 31)
	{
		if (ch >= 0 && ch < 39)
		{
			return 18;
		}
		else if (ch == 39)
		{
			return 26;
		}
		else if (ch >= 40 && ch < 92)
		{
			return 18;
		}
		else if (ch == 92)
		{
			return 27;
		}
		else if (ch >= 93)
		{
			return 18;
		}
	}

	return -1;
}

QueryTokenizer::QueryTokenizer() :
	FStartState(0)
{

}

void QueryTokenizer::Reset()
{
	FStartState = 0;
}

QueryToken QueryTokenizer::Process(const WCHAR * & AString)
{
	int CurrentState, CurrentAccept, NextState, NextAccept;
	const WCHAR * StringStart;
	std::wstring ProcessedString;
	QueryToken Result;
	bool Finish;

	CurrentState = FStartState;
	CurrentAccept = IsAcceptState[FStartState];
	StringStart = AString;
	Finish = false;

	do
	{
		if (*AString != '\0')
		{
			NextState = Transition(CurrentState, *AString);
			if (NextState != -1)
				NextAccept = IsAcceptState[NextState];
			else
				NextAccept = -1;
		}
		else
		{
			NextState = -1;
			NextAccept = -1;
		}

		if (NextState == -1 || (CurrentAccept != -1 && NextAccept == -1))
		{
			Finish = true;

			if (CurrentAccept != -1)
				FStartState = TargetState[CurrentState];
			switch (CurrentAccept)
			{
			case -1:
				{
					Result.tokenType = ttUnknown;
					if (StringStart == AString)
						Result.TokenStr = L"";
					else
						Result.TokenStr = std::wstring((WCHAR *)StringStart, (AString - StringStart));
					break;
				}
			default:
				{
					Result.tokenType = (QueryTokenType)CurrentAccept;
					if (StringStart == AString)
						Result.TokenStr = L"";
					else
						Result.TokenStr = std::wstring((WCHAR *)StringStart, (AString - StringStart));
					break;
				}
			}
		}
		else
		{
			CurrentState = NextState;
			CurrentAccept = NextAccept;
			AString++;
		}
	}
	while (!Finish);

return Result;
}

std::wstring QueryTokenizer::TokenTypeToStr(QueryTokenType tokenType)
{
	switch(tokenType)
	{
	case ttArrayIndex:
		{
			return L"ArrayIndex";
			break;
		}
	case ttArrayMember:
		{
			return L"ArrayMember";
			break;
		}
	case ttDot:
		{
			return L"Dot";
			break;
		}
	case ttMember:
		{
			return L"Member";
			break;
		}
	case ttUnknown:
		{
			return L"Unknown";
			break;
		}
	default:
		return L"Not recognized token";
	}
}
