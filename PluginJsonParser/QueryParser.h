#pragma once
#include <string>
#include <vector>

class ParsingException : public std::exception 
{
public:
	ParsingException(const char* const message);
};

class BaseQueryPart
{
public:
	BaseQueryPart();
	virtual ~BaseQueryPart();
};

class MemberAccessQueryPart : public BaseQueryPart
{
private:
	const std::wstring memberName;

public:
	MemberAccessQueryPart(std::wstring memberName);

	const std::wstring& getMemberName();
};

class ArrayItemAccessQueryPart : public BaseQueryPart
{
private:
	unsigned int index;

public:
	ArrayItemAccessQueryPart(unsigned int index);

	const unsigned int getIndex();
};

class QueryParser
{
public:
	static std::vector<std::shared_ptr<BaseQueryPart>> parseQuery(std::wstring query);
};

