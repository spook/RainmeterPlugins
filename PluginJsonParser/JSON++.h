/*
Module : JSON++.h
Purpose: Defines the interface for a set of classes to support JSON parsing and creation. The class is based
         in part on the SimpleJSON C++ class of Mike Anchor available at https://github.com/MJPA/SimpleJSON
History: PJN / 25-11-2017 1. Initial public release
         PJN / 10-01-2018 1. Updated copyright details.
                          2. Added support for JSON Pointer (https://tools.ietf.org/html/rfc6901) support to the code
                          via a new CValue::JSONPointer method.
                          3. Made a number of methods of the CValue class static as they did not need to be non-static
         PJN / 29-05-2018 1. Fixed a number of C++ core guidelines compiler warnings.
         PJN / 30-06-2018 1. Reworked the implementation to use std::variant. This does mean that the code now requires
                          at least the /std:c++17 compiler setting. This has resulted in the module shrinking by c. 160 
                          lines. Testing shows the performance remains the same as before.
         PJN / 31-07-2018 1. Reworked code to use Windows API calls MultiByteToWideChar API  for the UTF8 to UTF16 
                          conversion rather than the deprecated C17 deprecated codecvt header.
         PJN / 02-09-2018 1. Fixed a number of compiler warnings when using VS 2017 15.8.2
         PJN / 22-04-2019 1. Updated copyright details
                          2. Updated the code to clean compile on VC 2019
         PJN / 03-08-2019 1. Added comprehensive SAL annotations to all the code
         PJN / 29-01-2020 1. Updated copyright details.
                          2. Fixed more Clang-Tidy static code analysis warnings in the code.
         PJN / 23-05-2020 1. Added const versions of the CValue::Child methods.
                          2. Added [[nodiscard]] to a number of CValue methods.

Copyright (c) 2017 - 2020 by PJ Naughter (Web: www.naughter.com, Email: pjna@naughter.com)

All rights reserved.

Copyright / Usage Details:

You are allowed to include the source code in any product (commercial, shareware, freeware or otherwise) 
when your product is released in binary form. You are allowed to modify the source code in any way you want 
except you cannot modify the copyright details at the top of each module. If you want to distribute source 
code with your application, then you are only allowed to distribute versions released by the author. This is 
to maintain a single distribution point for the source code. 

*/


///////////////////////// Macros / Defines ////////////////////////////////////

#pragma once

#ifndef __JSONPP_H__
#define __JSONPP_H__


///////////////////////// Includes ////////////////////////////////////////////

#include <windows.h>
#include <string>
#include <vector>
#include <map>
#include <cassert>
#include <sstream>
#include <cmath>
#include <algorithm>
#include <locale>
#include <type_traits>
#include <variant>


///////////////////////// Implementation  /////////////////////////////////////

//Namespaces
namespace JSONPP
{


//Forward references
class CValue;

//Typedefs
using Object = std::map<std::wstring, CValue>;
using Array = std::vector<CValue>;

//Enums
enum class PARSE_EXCEPTION_REASON
{
  ENCODED_UNICODE_CHARACTER_TOO_SHORT               = 0,
  UNEXPECTED_CHARACTER_IN_ENCODED_UNICODE_CHARACTER = 1,
  UNEXPECTED_CHARACTER_IN_STRING                    = 2,
  UNEXPECTED_CHARACTER_IN_NUMBER                    = 3,
  UNEXPECTED_CHARACTER_IN_ARRAY                     = 4,
  UNEXPECTED_CHARACTER_IN_OBJECT                    = 5,
  UNEXPECTED_CHARACTER_IN_JSON                      = 6,
  UNEXPECTED_CHARACTER_IN_JSON_POINTER              = 7, //Only thrown by JSONPointer method
  COULD_NOT_FIND_CHILD_IN_OBJECT                    = 8, //Only thrown by JSONPointer method
  COULD_NOT_FIND_CHILD_IN_ARRAY                     = 9, //Only thrown by JSONPointer method
  CURRENT_ITEM_IS_NOT_OBJECT_OR_ARRAY               = 10 //Only thrown by JSONPointer method
};

enum ENCODE_FLAGS
{
  NONE                                      = 0x0000,
  TABS                                      = 0x0001,
  ONE_SPACE                                 = 0x0002,
  TWO_SPACES                                = 0x0004,
  THREE_SPACES                              = 0x0008,
  FOUR_SPACES                               = 0x0010,
  WHITESPACE_BETWEEN_OBJECT_NAME_AND_VALUES = 0x0020,
  WHITESPACE_AFTER_VALUE                    = 0x0040,
  WHITESPACE_BEFORE_VALUE                   = 0x0080,
  INDENT_SUB_OBJECTS                        = 0x0100,
  INDENT_SUB_ARRAYS                         = 0x0200,
  PRETTY_PRINT_ONE_SPACE                    = 0x0302, //A combination of INDENT_SUB_ARRAYS, INDENT_SUB_OBJECTS & ONE_SPACE
  ENCODE_NON_ASCII_CHARACTERS               = 0x0400,
  ENCODE_ALL_UNICODE_CHARACTERS             = 0x0800
};

enum class TYPE
{
  NULL_VALUE = 0,
  STRING     = 1,
  NUMBER     = 2,
  OBJECT     = 3,
  ARRAY      = 4,
  BOOLEAN    = 5
};

//Classes
class CParseException : public std::exception
{
public:
//Constructors / Destructors
  CParseException(_In_ PARSE_EXCEPTION_REASON reason, _In_ std::size_t nIndex) noexcept : m_Reason(reason),
                                                                                          m_nIndex(nIndex)
  {
  }

//Member variables
  PARSE_EXCEPTION_REASON m_Reason; //The reason for the parsing error
  std::size_t            m_nIndex; //The 0 based index from the begining of the string where the parsing error occurred
#pragma warning(suppress: 26495)
};

class CValue
{
public:
//Constructors / Destructors
  CValue() noexcept : m_Type(TYPE::NULL_VALUE)
  {
  }

  ~CValue() = default;
  CValue(const CValue&) = default;
  CValue(CValue&& value) = default;

#pragma warning(suppress: 26440)
  CValue(_In_ const std::wstring& sString) : m_Data(sString),
                                             m_Type(TYPE::STRING)
  {
  }

#pragma warning(suppress: 26440)
  CValue(_In_ std::wstring&& sString) : m_Data(sString),
                                        m_Type(TYPE::STRING)
  {
  }

#pragma warning(suppress: 26440)
  CValue(_In_z_ const wchar_t* pszString) : m_Data(std::wstring(pszString)),
                                            m_Type(TYPE::STRING)
  {
  }

#pragma warning(suppress: 26440)
  CValue(_In_ double fNumber) : m_Data(fNumber),
                                m_Type(TYPE::NUMBER)
  {
  }

#pragma warning(suppress: 26440)
  CValue(_In_ long nNumber) : m_Data(static_cast<double>(nNumber)),
                              m_Type(TYPE::NUMBER)
  {
  }

#pragma warning(suppress: 26440)
  CValue(_In_ const Object& object) : m_Data(object),
                                      m_Type(TYPE::OBJECT)
  {
  }

  CValue(_In_ Object&& object) noexcept : m_Data(object),
                                          m_Type(TYPE::OBJECT)
  {
  }

#pragma warning(suppress: 26440)
  CValue(_In_ const Array& array) : m_Data(array),
                                    m_Type(TYPE::ARRAY)
  {
  }

#pragma warning(suppress: 26440)
  CValue(_In_ Array&& array) : m_Data(array),
                               m_Type(TYPE::ARRAY)
  {
  }

#pragma warning(suppress: 26440)
  CValue(_In_ bool bValue) : m_Data(bValue),
                             m_Type(TYPE::BOOLEAN)
  {
  }

//Methods
  void SetNull() noexcept
  {
    m_Type = TYPE::NULL_VALUE;
  }

  CValue& operator=(_In_ const std::wstring& sString)
  {
    m_Data = sString;
    m_Type = TYPE::STRING;
    return *this;
  }

#pragma warning(suppress: 26440)
  CValue& operator=(_In_z_ const wchar_t* pszString)
  {
    m_Data = pszString;
    m_Type = TYPE::STRING;
    return *this;
  }

#pragma warning(suppress: 26440)
  CValue& operator=(_In_ std::wstring&& sString)
  {
    m_Data = std::move(sString);
    m_Type = TYPE::STRING;
    return *this;
  }

#pragma warning(suppress: 26440)
  CValue& operator=(_In_ double fNumber)
  {
    m_Data = fNumber;
    m_Type = TYPE::NUMBER;
    return *this;
  }

#pragma warning(suppress: 26440)
  CValue& operator=(_In_ long nNumber)
  {
    m_Data = static_cast<double>(nNumber);
    m_Type = TYPE::NUMBER;
    return *this;
  }

  CValue& operator=(_In_ const Object& object)
  {
    m_Data = object;
    m_Type = TYPE::OBJECT;
    return *this;
  }

  CValue& operator=(_In_ Object&& object)
  {
    m_Data = std::move(object);
    m_Type = TYPE::OBJECT;
    return *this;
  }

  CValue& operator=(_In_ const Array& array)
  {
    m_Data = array;
    m_Type = TYPE::ARRAY;
    return *this;
  }

#pragma warning(suppress: 26440)
  CValue& operator=(_In_ Array&& array)
  {
    m_Data = std::move(array);
    m_Type = TYPE::ARRAY;
    return *this;
  }

#pragma warning(suppress: 26440)
  CValue& operator=(_In_ bool bValue)
  {
    m_Data = bValue;
    m_Type = TYPE::BOOLEAN;
    return *this;
  }

  CValue& operator=(_In_ const CValue& value)
  {
    switch (value.m_Type)
    {
      case TYPE::NULL_VALUE:
      {
        //Nothing to do
        break;
      }
      case TYPE::STRING:
      {
        m_Data = value.AsString();
        break;
      }
      case TYPE::NUMBER:
      {
        m_Data = value.AsNumber();
        break;
      }
      case TYPE::OBJECT:
      {
        m_Data = value.AsObject();
        break;
      }
      case TYPE::ARRAY:
      {
        m_Data = value.AsArray();
        break;
      }
      case TYPE::BOOLEAN:
      {
        m_Data = value.AsBoolean();
        break;
      }
      default:
      {
      #ifdef _DEBUG
        assert(false);
      #endif //#ifdef _DEBUG
        break;
      }
    }
    m_Type = value.m_Type;
    return *this;
  }

#pragma warning(suppress: 26439)
  CValue& operator=(_In_ CValue&& value)
  {
    switch (value.m_Type)
    {
      case TYPE::NULL_VALUE:
      {
        break;
      }
      case TYPE::STRING:
      {
        m_Data = std::move(value.AsString());
        break;
      }
      case TYPE::NUMBER:
      {
        m_Data = value.AsNumber();
        break;
      }
      case TYPE::OBJECT:
      {
        m_Data = std::move(value.AsObject());
        break;
      }
      case TYPE::ARRAY:
      {
        m_Data = std::move(value.AsArray());
        break;
      }
      case TYPE::BOOLEAN:
      {
        m_Data = value.AsBoolean();
        break;
      }
      default:
      {
      #ifdef _DEBUG
        assert(false);
      #endif //#ifdef _DEBUG
        break;
      }
    }
    m_Type = value.m_Type;
    return *this;
  }

  [[nodiscard]] inline TYPE Type() const noexcept
  {
    return m_Type;
  }

  [[nodiscard]] inline bool IsNull() const noexcept
  {
    return m_Type == TYPE::NULL_VALUE;
  }

  [[nodiscard]] inline bool IsString() const noexcept
  {
    return m_Type == TYPE::STRING;
  }

  [[nodiscard]] inline bool IsNumber() const noexcept
  {
    return m_Type == TYPE::NUMBER;
  }

  [[nodiscard]] inline bool IsArray() const noexcept
  {
    return m_Type == TYPE::ARRAY;
  }

  [[nodiscard]] inline bool IsObject() const noexcept
  {
    return m_Type == TYPE::OBJECT;
  }

  [[nodiscard]] inline bool IsBoolean() const noexcept
  {
    return m_Type == TYPE::BOOLEAN;
  }

  [[nodiscard]] inline const std::wstring AsString() const
  {
  //Validate our parameters
  #ifdef _DEBUG
    assert(m_Type == TYPE::STRING);
  #endif //#ifdef _DEBUG

    return std::get<std::wstring>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline std::wstring& AsString()
  {
  //Validate our parameters
  #ifdef _DEBUG
    assert(m_Type == TYPE::STRING);
  #endif //#ifdef _DEBUG

    return std::get<std::wstring>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline double AsNumber() const
  {
    //Validate our parameters
  #ifdef _DEBUG
    assert(m_Type == TYPE::NUMBER);
  #endif //#ifdef _DEBUG

    return std::get<double>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline double& AsNumber()
  {
    //Validate our parameters
  #ifdef _DEBUG
assert(m_Type == TYPE::NUMBER);
#endif //#ifdef _DEBUG

    return std::get<double>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline const Array& AsArray() const
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::ARRAY);
#endif //#ifdef _DEBUG

    return std::get<Array>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline Array& AsArray()
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::ARRAY);
#endif //#ifdef _DEBUG

    return std::get<Array>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline const Object& AsObject() const
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::OBJECT);
#endif //#ifdef _DEBUG

    return std::get<Object>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline Object& AsObject()
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::OBJECT);
#endif //#ifdef _DEBUG

    return std::get<Object>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline bool AsBoolean() const
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::BOOLEAN);
#endif //#ifdef _DEBUG

    return std::get<bool>(m_Data);
  }

#pragma warning(suppress: 26440)
  [[nodiscard]] inline bool& AsBoolean()
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::BOOLEAN);
#endif //#ifdef _DEBUG

    return std::get<bool>(m_Data);
  }

  void Parse(_In_z_ const char* pszJSON, _In_ int nStringReserveSize = -1)
  {
    std::wstring sWideString(UTF82W(pszJSON));

    //Delegate to the other version of the method
    Parse(sWideString.c_str(), nStringReserveSize);
  }

  void Parse(_In_z_ const wchar_t* pszJSON, _In_ int nStringReserveSize = -1)
  {
    const wchar_t* pszCurrentChar = pszJSON;

    //Skip any leading whitespace
    SkipWhitespaceNullNotOK(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_JSON, pszCurrentChar);

    //Delegate the main work to the helper method
    ParseHelper(pszJSON, pszCurrentChar, nStringReserveSize);

    //Should only be whitespace left now
    SkipWhitespaceOnlyNullOK(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_JSON, pszCurrentChar);
  }

  CValue* JSONPointer(_In_z_ const char* pszJSONPointer)
  {
    std::wstring sWideString(UTF82W(pszJSONPointer));

    //Delegate to the other version of the method
#pragma warning(suppress: 26487)
    return JSONPointer(sWideString.c_str());
  }

  CValue* JSONPointer(_In_z_ const wchar_t* pszJSONPointer)
  {
    //What will be the return value from this method
#pragma warning(suppress: 26429)
    CValue* pCurrent = this;

    //Handle the special case of an empty string
#pragma warning(suppress: 26429)
    const wchar_t* pszCurrentChar = pszJSONPointer;
    if (*pszCurrentChar == L'\0')
      return pCurrent;

    bool bEndOfTokens = false;
    while (!bEndOfTokens)
    {
      //Skip over the "/"
      if (*pszCurrentChar != L'/')
        throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_JSON_POINTER, pszCurrentChar - pszJSONPointer);
#pragma warning(suppress: 26481)
      ++pszCurrentChar;

      //Extract the next JSON pointer token
#pragma warning(suppress: 26486)
      std::wstring sToken(ExtractJSONPointerString(pszJSONPointer, pszCurrentChar));

      //Navigate to the next node based on whether the current node is a JSON object or a JSON array
      if (pCurrent->IsObject())
      {
        if (!pCurrent->HasChild(sToken.c_str()))
          throw CParseException(PARSE_EXCEPTION_REASON::COULD_NOT_FIND_CHILD_IN_OBJECT, pszCurrentChar - pszJSONPointer);
        pCurrent = &pCurrent->Child(sToken.c_str());
      }
      else if (pCurrent->IsArray())
      {
        const int nIndex = ExtractJSONPointerInt(sToken.c_str());
        if (!pCurrent->HasChild(nIndex))
          throw CParseException(PARSE_EXCEPTION_REASON::COULD_NOT_FIND_CHILD_IN_ARRAY, pszCurrentChar - pszJSONPointer);
        pCurrent = &pCurrent->Child(nIndex);
      }
      else
        throw CParseException(PARSE_EXCEPTION_REASON::CURRENT_ITEM_IS_NOT_OBJECT_OR_ARRAY, pszCurrentChar - pszJSONPointer);

      //Prepare for the next time around
      bEndOfTokens = (*pszCurrentChar == L'\0');
    }

    return pCurrent;
  }

  [[nodiscard]] std::wstring Encode(_In_ unsigned long nFlags = ENCODE_FLAGS::NONE) const
  {
    return Encode(nFlags, 0);
  }

  [[nodiscard]] std::size_t CountChildren() const
  {
    if (m_Type == TYPE::OBJECT)
      return AsObject().size();
    else if (m_Type == TYPE::ARRAY)
      return AsArray().size();
    else
      return 0;
  }

  [[nodiscard]] bool HasChild(_In_ std::size_t nIndex) const
  {
    if (m_Type == TYPE::ARRAY)
      return nIndex < AsArray().size();
    else
      return false;
  }

  [[nodiscard]] bool HasChild(_In_z_ const wchar_t* pszName) const
  {
    if (m_Type == TYPE::OBJECT)
    {
      const Object& object = AsObject();
      return object.find(pszName) != object.cend();
    }
    else
      return false;
  }

  [[nodiscard]] CValue& Child(_In_ std::size_t nIndex)
  {
    //Validate our parameters
  #ifdef _DEBUG
    assert(m_Type == TYPE::ARRAY);
  #endif

#pragma warning(suppress: 26446)
    return AsArray()[nIndex];
  }

  [[nodiscard]] const CValue& Child(std::size_t nIndex) const
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::ARRAY);
#endif

#pragma warning(suppress: 26446)
    return AsArray()[nIndex];
  }

  [[nodiscard]] CValue& Child(_In_z_ const wchar_t* pszName)
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::OBJECT);
#endif

#pragma warning(suppress: 26487 26489)
    return AsObject().find(pszName)->second;
  }

  [[nodiscard]] const CValue& Child(_In_z_ const wchar_t* pszName) const
  {
    //Validate our parameters
#ifdef _DEBUG
    assert(m_Type == TYPE::OBJECT);
#endif

#pragma warning(suppress: 26487 26489)
    return AsObject().find(pszName)->second;
  }

  [[nodiscard]] std::vector<std::wstring> ObjectKeys() const
  {
    //What will be the return value from this method
    std::vector<std::wstring> keys;

    if (m_Type == TYPE::OBJECT)
    {
      const Object& object = AsObject();
      keys.reserve(object.size());
      for (auto& iter : object)
#pragma warning(suppress: 26489)
        keys.push_back(iter.first);
    }

    return keys;
  }

  static std::wstring UTF82W(_In_z_ const char* pszText)
  {
    //First call the function to determine how much space we need to allocate
    int nWideLength = MultiByteToWideChar(CP_UTF8, 0, pszText, -1, nullptr, 0);

    //Now recall with the buffer to get the converted text
    std::wstring sWideString;
    sWideString.resize(nWideLength);
    nWideLength = MultiByteToWideChar(CP_UTF8, 0, pszText, -1, sWideString.data(), nWideLength);
    sWideString.resize(nWideLength);

    return sWideString;
  }

protected:
//Member variables
  std::variant<bool, double, std::wstring, Object, Array> m_Data;
  TYPE m_Type;

//Methods
#pragma warning(suppress: 26429)
  void ParseHelper(_In_z_ const wchar_t* pszJSON, _Inout_ const wchar_t*& pszCurrentChar, _In_ int nStringReserveSize)
  {
    const size_t nLen = wcslen(pszCurrentChar);
    if (*pszCurrentChar == L'"') //Is the data a string
    {
#pragma warning(suppress: 26481 26486)
      *this = ExtractString(pszJSON, ++pszCurrentChar, nStringReserveSize);
      return;
    }
    else if ((nLen >= 4) && (_wcsnicmp(pszCurrentChar, L"true", 4) == 0)) //Is the data true
    {
      *this = true;
#pragma warning(suppress: 26481 26487)
      pszCurrentChar += 4;
      return;
    }
    else if ((nLen >= 5) && (_wcsnicmp(pszCurrentChar, L"false", 5) == 0)) //Is the data false
    {
      *this = false;
#pragma warning(suppress: 26481)
      pszCurrentChar += 5;
      return;
    }
    else if ((nLen >= 4) && (_wcsnicmp(pszCurrentChar, L"null", 4) == 0)) //Is the data null
    {
      SetNull();
#pragma warning(suppress: 26481)
      pszCurrentChar += 4;
      return;
    }
    else if ((*pszCurrentChar == L'-') || (*pszCurrentChar >= L'0' && *pszCurrentChar <= L'9')) //Is the data a number
    {
      //Is the value a negative number
      const bool bNegative = (*pszCurrentChar == L'-');
      if (bNegative)
#pragma warning(suppress: 26481)
        ++pszCurrentChar;

      //Parse the whole part of the number
      double fNumber = 0;
#pragma warning(suppress: 26489)
      if (*pszCurrentChar == L'0')
#pragma warning(suppress: 26481 26487)
        ++pszCurrentChar;
      else
#pragma warning(suppress: 26486)
        fNumber = ExtractInt(pszJSON, pszCurrentChar);

      //There could be a decimal point now
#pragma warning(suppress: 26489)
      if (*pszCurrentChar == L'.')
      {
#pragma warning(suppress: 26481)
        ++pszCurrentChar;

        //Pull out the decimal part and add it to the number
#pragma warning(suppress: 26486)
        fNumber += ExtractDecimal(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_NUMBER, pszCurrentChar);
      }

      //There could be an exponent now
#pragma warning(suppress: 26489)
      if (*pszCurrentChar == L'e' || *pszCurrentChar == L'E')
      {
#pragma warning(suppress: 26481)
        ++pszCurrentChar;

        //Check the sign of the exponent
        bool bNegativeExponent = false;
#pragma warning(suppress: 26489)
        if ((*pszCurrentChar == L'-') || (*pszCurrentChar == L'+'))
        {
#pragma warning(suppress: 26489)
          bNegativeExponent = (*pszCurrentChar == L'-');
#pragma warning(suppress: 26481)
          ++pszCurrentChar;
        }

        //Parse the exponent value
#pragma warning(suppress: 26486)
        const int nExponent = static_cast<int>(ExtractInt(pszJSON, pszCurrentChar));
        for (int i=0; i<nExponent; i++)
          fNumber = bNegativeExponent ? (fNumber / 10.0) : (fNumber * 10.0);
      }

      //Handle the case of the number being negative
      if (bNegative)
        fNumber *= -1;

      *this = fNumber;
    }
    else if (*pszCurrentChar == L'{') //Is the data an object
    {
#pragma warning(suppress: 26481 26487)
      ++pszCurrentChar;

      Object object;
#pragma warning(suppress: 26489)
      while (*pszCurrentChar != L'\0')
      {
        //Skip whitespace
#pragma warning(suppress: 26486)
        SkipWhitespaceNullNotOK(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_OBJECT, pszCurrentChar);

        //Handle the special case of an empty object
        if ((object.size() == 0) && (*pszCurrentChar == L'}'))
        {
#pragma warning(suppress: 26481)
          ++pszCurrentChar;
          *this = std::move(object);
          return;
        }

        //Parse the child element name now
        if (*pszCurrentChar != L'\"')
          throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_OBJECT, pszCurrentChar - pszJSON);
#pragma warning(suppress: 26481)
        ++pszCurrentChar;
#pragma warning(suppress: 26486)
        std::wstring sName(ExtractString(pszJSON, pszCurrentChar, nStringReserveSize));

        //Skip whitespace
        SkipWhitespaceNullNotOK(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_OBJECT, pszCurrentChar);

        //There should be a ":" now
        if (*pszCurrentChar != L':')
          throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_OBJECT, pszCurrentChar - pszJSON);
#pragma warning(suppress: 26481)
        ++pszCurrentChar;

        //Skip whitespace
#pragma warning(suppress: 26486)
        SkipWhitespaceNullNotOK(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_OBJECT, pszCurrentChar);

        //Parse the child element value now
        CValue childElement;
        childElement.ParseHelper(pszJSON, pszCurrentChar, nStringReserveSize);
        object[std::move(sName)] = std::move(childElement);

        //Skip whitespace
        SkipWhitespaceNullNotOK(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_OBJECT, pszCurrentChar);

        //Are we at the end of the object yet?
        if (*pszCurrentChar == L'}')
        {
#pragma warning(suppress: 26481 26487)
          ++pszCurrentChar;
          *this = std::move(object);
          return;
        }

        //There should be a comma now
        if (*pszCurrentChar != L',')
          throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_OBJECT, pszCurrentChar - pszJSON);

        //Prepare for the next time around
#pragma warning(suppress: 26481)
        ++pszCurrentChar;
      }
    }
    else if (*pszCurrentChar == L'[') //Is the data an array
    {
#pragma warning(suppress: 26481 26487)
      ++pszCurrentChar;

      Array array;
#pragma warning(suppress: 26489)
      while (*pszCurrentChar != L'\0')
      {
        //Skip any leading whitespace
#pragma warning(suppress: 26486)
        SkipWhitespaceNullNotOK(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_ARRAY, pszCurrentChar);

        //Handle the special case of an empty array
        if ((array.size() == 0) && (*pszCurrentChar == L']'))
        {
#pragma warning(suppress: 26481)
          ++pszCurrentChar;
          *this = std::move(array);
          return;
        }

        //Parse the child element value now
        CValue childElement;
        childElement.ParseHelper(pszJSON, pszCurrentChar, nStringReserveSize);
        array.push_back(std::move(childElement));

        //Skip any trailing whitespace
        SkipWhitespaceNullNotOK(pszJSON, PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_ARRAY, pszCurrentChar);

        //Are we at the end of the array yet?
        if (*pszCurrentChar == L']')
        {
#pragma warning(suppress: 26481 26487)
          ++pszCurrentChar;
          *this = std::move(array);
          return;
        }

        //There should be a comma now
        if (*pszCurrentChar != L',')
          throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_ARRAY, pszCurrentChar - pszJSON);

        //Prepare for the next time around
#pragma warning(suppress: 26481)
        ++pszCurrentChar;
      }
    }
    else
      throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_JSON, pszCurrentChar - pszJSON);
  }

  static void HandleBeforeValue(_Inout_ std::wstring& s, _In_ unsigned long nFlags)
  {
    if (nFlags & WHITESPACE_BEFORE_VALUE)
    {
      if (nFlags & TABS)
        s += L"\t";
      else if (nFlags & FOUR_SPACES)
        s += L"    ";
      else if (nFlags & THREE_SPACES)
        s += L"   ";
      else if (nFlags & TWO_SPACES)
        s += L"  ";
      else if (nFlags & ONE_SPACE)
        s += L" ";
    }
  }

  static void HandleAfterValue(_Inout_ std::wstring& s, _In_ unsigned long nFlags)
  {
    if (nFlags & WHITESPACE_AFTER_VALUE)
    {
      if (nFlags & TABS)
        s += L"\t";
      else if (nFlags & FOUR_SPACES)
        s += L"    ";
      else if (nFlags & THREE_SPACES)
        s += L"   ";
      else if (nFlags & TWO_SPACES)
        s += L"  ";
      else if (nFlags & ONE_SPACE)
        s += L" ";
    }
  }

  static void EncodeEncodedDigit(_Inout_ std::wstring& s, _In_ int nDigit)
  {
    if ((nDigit >= 0) && (nDigit <= 9))
#pragma warning(suppress: 26472)
      s += static_cast<wchar_t>(L'0' + nDigit);
    else
#pragma warning(suppress: 26472)
      s += static_cast<wchar_t>(L'A' + (nDigit - 10));
  }

  static void HandleEncodeCharacter(_Inout_ std::wstring& s, _In_ wchar_t chr)
  {
    s += L"\\u";
    EncodeEncodedDigit(s, (chr & 0xF000) >> 12);
#pragma warning(suppress: 26486 26489)
    EncodeEncodedDigit(s, (chr & 0x0F00) >> 8);
#pragma warning(suppress: 26486 26489)
    EncodeEncodedDigit(s, (chr & 0x00F0) >> 4);
#pragma warning(suppress: 26486 26489)
    EncodeEncodedDigit(s, chr & 0x000F);
  }

  static std::wstring EncodeString(_In_ const std::wstring& string, _In_ unsigned long nFlags)
  {
    std::wstring sReturn;
    sReturn.reserve(string.size() + 2);
    sReturn = L"";
    HandleBeforeValue(sReturn, nFlags);
    sReturn += L"\"";
    for (auto& chr : string)
    {
      if (nFlags & ENCODE_ALL_UNICODE_CHARACTERS)
        HandleEncodeCharacter(sReturn, chr);
      else
      {
        if (chr == L'\"')
          sReturn += L"\\\"";
        else if (chr == L'\\')
          sReturn += L"\\\\";
        else if (chr == L'/')
          sReturn +=  L"\\/";
        else if (chr == L'\b')
          sReturn += L"\\b";
        else if (chr == L'\f')
          sReturn += L"\\f";
        else if (chr == L'\n')
          sReturn += L"\\n";
        else if (chr == L'\r')
          sReturn += L"\\r";
        else if (chr == L'\t')
          sReturn += L"\\t";
        else if (((chr < L' ') || (chr > L'~')) && (nFlags & ENCODE_NON_ASCII_CHARACTERS))
          HandleEncodeCharacter(sReturn, chr);
        else
            sReturn += chr;
      }
    }
    sReturn += L"\"";
    HandleAfterValue(sReturn, nFlags);
    return sReturn;
  }

#pragma warning(suppress: 26429)
  static void SkipWhitespaceNullNotOK(_In_z_ const wchar_t* pszJSON, _In_ PARSE_EXCEPTION_REASON parseError, _Inout_ const wchar_t*& pszCurrentChar)
  {
#pragma warning(suppress: 26489)
    while ((*pszCurrentChar != L'\0') && (*pszCurrentChar == L' ' || *pszCurrentChar == L'\t' || *pszCurrentChar == L'\r' || *pszCurrentChar == L'\n'))
#pragma warning(suppress: 26481 26487)
      ++pszCurrentChar;

#pragma warning(suppress: 26489)
    if (*pszCurrentChar == L'\0')
      throw CParseException(parseError, pszCurrentChar - pszJSON);
  }

#pragma warning(suppress: 26429)
  static void SkipWhitespaceOnlyNullOK(_In_z_ const wchar_t* pszJSON, _In_ PARSE_EXCEPTION_REASON parseError, _Inout_ const wchar_t*& pszCurrentChar)
  {
#pragma warning(suppress: 26489)
    while ((*pszCurrentChar != L'\0') && (*pszCurrentChar == L' ' || *pszCurrentChar == L'\t' || *pszCurrentChar == L'\r' || *pszCurrentChar == L'\n'))
#pragma warning(suppress: 26481 26487)
      ++pszCurrentChar;

#pragma warning(suppress: 26489)
    if (*pszCurrentChar != L'\0')
      throw CParseException(parseError, pszCurrentChar - pszJSON);
  }

#pragma warning(suppress: 26429)
  static double ExtractInt(_In_z_ const wchar_t* pszJSON, _Inout_ const wchar_t*& pszCurrentChar)
  {
    //We need at least one digit for an int
    if ((*pszCurrentChar < L'0') || (*pszCurrentChar > L'9'))
      throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_NUMBER, pszCurrentChar - pszJSON);

    //What will be the return value from this function
    double fNumber = 0;

#pragma warning(suppress: 26489)
    while ((*pszCurrentChar != L'\0') && (*pszCurrentChar >= L'0') && (*pszCurrentChar <= L'9'))
    {
#pragma warning(suppress: 26489)
      fNumber = (fNumber * 10) + (*pszCurrentChar - L'0');

      //Prepare for the next time around
#pragma warning(suppress: 26481 26487)
      ++pszCurrentChar;
    }

    return fNumber;
  }

#pragma warning(suppress: 26429)
  static double ExtractDecimal(_In_z_ const wchar_t* pszJSON, _In_ PARSE_EXCEPTION_REASON parseError, _Inout_ const wchar_t*& pszCurrentChar)
  {
    //We need at least one digit for a decimal
    if ((*pszCurrentChar < L'0') || (*pszCurrentChar > L'9'))
      throw CParseException(parseError, pszCurrentChar - pszJSON);

    //What will be the return value from this function
    double fDecimal = 0.0;

    double fFactor = 0.1;
#pragma warning(suppress: 26489)
    while ((*pszCurrentChar != L'\0') && (*pszCurrentChar >= L'0') && (*pszCurrentChar <= L'9'))
    {
#pragma warning(suppress: 26489)
      const int nDigit = (*pszCurrentChar - L'0');
      fDecimal = fDecimal + nDigit*fFactor;

      //Prepare for the next time around
#pragma warning(suppress: 26481 26487)
      ++pszCurrentChar;
      fFactor *= 0.1;
    }

    return fDecimal;
  }

#pragma warning(suppress: 26429)
  static std::wstring ExtractString(_In_z_ const wchar_t* pszJSON, _Inout_ const wchar_t*& pszCurrentChar, _In_ int nStringReserveSize)
  {
    //What will be the return value from this method
    std::wstring sReturn;
    if (nStringReserveSize != -1)
      sReturn.reserve(nStringReserveSize);
    sReturn = L"";

#pragma warning(suppress: 26489)
    while (*pszCurrentChar != L'\0')
    {
#pragma warning(suppress: 26489)
      wchar_t currentChar = *pszCurrentChar;

      if (currentChar == L'\\') //The string has been escaped
      {
#pragma warning(suppress: 26481)
        ++pszCurrentChar;

        switch (*pszCurrentChar)
        {
          case L'"':
          {
            currentChar = L'"';
            break;
          }
          case L'\\':
          {
            currentChar = L'\\';
            break;
          }
          case L'/':
          {
            currentChar = L'/';
            break;
          }
          case L'b':
          {
            currentChar = L'\b';
            break;
          }
          case L'f':
          {
            currentChar = L'\f';
            break;
          }
          case L'n':
          {
            currentChar = L'\n';
            break;
          }
          case L'r':
          {
            currentChar = L'\r';
            break;
          }
          case L't':
          {
            currentChar = L'\t';
            break;
          }
          case L'u':
          {
            //We need 4 digits after the "u"
#pragma warning(suppress: 26486)
            if (wcslen(pszCurrentChar) < 5)
              throw CParseException(PARSE_EXCEPTION_REASON::ENCODED_UNICODE_CHARACTER_TOO_SHORT, pszCurrentChar - pszJSON);

            //Parse out the 4 digits after the "u"
            currentChar = 0;
            for (int i=0; i<4; i++)
            {
#pragma warning(suppress: 26481)
              ++pszCurrentChar;
              currentChar <<= 4;
#pragma warning(suppress: 26489)
              if ((*pszCurrentChar >= '0') && (*pszCurrentChar <= '9'))
                currentChar |= (*pszCurrentChar - '0');
#pragma warning(suppress: 26489)
              else if ((*pszCurrentChar >= 'A') && (*pszCurrentChar <= 'F'))
                currentChar |= (10 + (*pszCurrentChar - 'A'));
#pragma warning(suppress: 26489)
              else if ((*pszCurrentChar >= 'a') && (*pszCurrentChar <= 'f'))
                currentChar |= (10 + (*pszCurrentChar - 'a'));
              else
                throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_ENCODED_UNICODE_CHARACTER, pszCurrentChar - pszJSON);
            }
            break;
          }
          default:
          {
            throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_STRING, pszCurrentChar - pszJSON);
            break;
          }
        }
      }
      else if (currentChar == L'"') //The end of the string
      {
#pragma warning(suppress: 26481 26487)
        ++pszCurrentChar;
        return sReturn;
      }
      else if (currentChar < L' ') //A character which is not allowed
      {
        throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_STRING, pszCurrentChar - pszJSON);
      }

      sReturn += currentChar;
#pragma warning(suppress: 26481)
      ++pszCurrentChar;
    }

    throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_STRING, pszCurrentChar - pszJSON);
  }

  static std::wstring IndentString(_In_ unsigned long nFlags, _In_ std::size_t nIndentLevel)
  {
    if (nFlags & FOUR_SPACES)
      return std::wstring(nIndentLevel * 4, L' ');
    else if (nFlags & THREE_SPACES)
      return std::wstring(nIndentLevel * 3, L' ');
    else if (nFlags & TWO_SPACES)
      return std::wstring(nIndentLevel * 2, L' ');
    else if (nFlags & ONE_SPACE)
      return std::wstring(nIndentLevel, L' ');
    else if (nFlags & TABS)
      return std::wstring(nIndentLevel, L'\t');
    else
      return std::wstring();
  }

  [[nodiscard]] std::wstring Encode(_In_ unsigned long nFlags, _In_ std::size_t nIndentLevel) const
  {
    //What will be the return value from this method
    std::wstring sString;

    const std::size_t nNextIndentLevel = nIndentLevel + 1;
    const std::wstring sIndent1(IndentString(nFlags, nIndentLevel));
    const std::wstring sIndent2(IndentString(nFlags, nNextIndentLevel));

    switch (m_Type)
    {
      case TYPE::NULL_VALUE:
      {
        HandleBeforeValue(sString, nFlags);
        sString += L"null";
        HandleAfterValue(sString, nFlags);
        break;
      }
      case TYPE::BOOLEAN:
      {
        HandleBeforeValue(sString, nFlags);
        if (AsBoolean())
          sString += L"true";
        else
          sString += L"false";
        HandleAfterValue(sString, nFlags);
        break;
      }
      case TYPE::NUMBER:
      {
        HandleBeforeValue(sString, nFlags);
        const double fNumber = AsNumber();
        if (std::isinf(fNumber) || std::isnan(fNumber)) //Treat infinity and NaN as a JSON null
          sString += L"null";
        else
        {
          std::wstringstream ss;
          ss.precision(15);
          ss << fNumber;
          sString += ss.str();
        }
        HandleAfterValue(sString, nFlags);
        break;
      }
      case TYPE::STRING:
      {
        sString = EncodeString(AsString(), nFlags);
        break;
      }
      case TYPE::OBJECT:
      {
        HandleBeforeValue(sString, nFlags);
        if (nIndentLevel && (nFlags & INDENT_SUB_OBJECTS))
        {
          sString += L"{\n";
          sString += sIndent2;
        }
        else
          sString += L"{";
        const Object& object = AsObject();
        auto iter = object.cbegin();
        while (iter != object.cend())
        {
          sString += EncodeString(iter->first, nFlags);
          if (nFlags & WHITESPACE_BETWEEN_OBJECT_NAME_AND_VALUES)
          {
            if (nFlags & TABS)
              sString += L":\t";
            else if (nFlags & FOUR_SPACES)
              sString += L":    ";
            else if (nFlags & THREE_SPACES)
              sString += L":   ";
            else if (nFlags & TWO_SPACES)
              sString += L":  ";
            else if (nFlags & ONE_SPACE)
              sString += L": ";
            else
              sString += L":";
          }
          else
            sString += L":";
          sString += iter->second.Encode(nFlags, nNextIndentLevel);
          if (++iter != object.cend())
            sString += L",";
        }
        if (nIndentLevel && (nFlags & INDENT_SUB_OBJECTS))
        {
          sString += L"\n" ;
          sString += sIndent1;
          sString += L"}";
        }
        else
          sString += L"}";
        HandleAfterValue(sString, nFlags);
        break;
      }
      case TYPE::ARRAY:
      {
        HandleBeforeValue(sString, nFlags);
        if (nIndentLevel && (nFlags & INDENT_SUB_ARRAYS))
        {
          sString += L"[\n";
          sString += sIndent2;
        }
        else
          sString += L"[";
        const Array& array = AsArray();
        auto iter = array.cbegin();
        while (iter != array.cend())
        {
          sString += iter->Encode(nFlags, nNextIndentLevel);
          if (++iter != array.cend())
            sString += L",";
        }
        if (nIndentLevel && (nFlags & INDENT_SUB_ARRAYS))
        {
          sString += L"\n";
          sString += sIndent1;
          sString += L"]";
        }
        else
          sString += L"]";
        HandleAfterValue(sString, nFlags);
        break;
      }
      default:
      {
      #ifdef _DEBUG
        assert(false);
      #endif //#ifdef _DEBUG
        break;
      }
    }

    return sString;
  }

#pragma warning(suppress: 26429)
  static int ExtractJSONPointerInt(_In_z_ const wchar_t* pszJSON)
  {
    //We need at least one digit for an int
    if ((*pszJSON < L'0') || (*pszJSON > L'9'))
      throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_NUMBER, 0);

    //What will be the return value from this function
    int nNumber = 0;

#pragma warning(suppress: 26489)
    while ((*pszJSON != L'\0') && (*pszJSON >= L'0') && (*pszJSON <= L'9'))
    {
#pragma warning(suppress: 26489)
      nNumber = (nNumber * 10) + (*pszJSON - L'0');

      //Prepare for the next time around
#pragma warning(suppress: 26481)
      ++pszJSON;
    }

    return nNumber;
  }

#pragma warning(suppress: 26429)
  static std::wstring ExtractJSONPointerString(_In_z_ const wchar_t* pszJSONPointer, _Inout_ const wchar_t*& pszCurrentChar)
  {
    //What will be the return value from this method
    std::wstring sReturn(L"");

#pragma warning(suppress: 26489)
    while (*pszCurrentChar != L'\0')
    {
#pragma warning(suppress: 26489)
      wchar_t currentChar = *pszCurrentChar;

      if (currentChar == L'\\') //The string has been escaped
      {
#pragma warning(suppress: 26481)
        ++pszCurrentChar;

        switch (*pszCurrentChar)
        {
          case L'"':
          {
            currentChar = L'"';
            break;
          }
          case L'\\':
          {
            currentChar = L'\\';
            break;
          }
          case L'/':
          {
            currentChar = L'/';
            break;
          }
          case L'b':
          {
            currentChar = L'\b';
            break;
          }
          case L'f':
          {
            currentChar = L'\f';
            break;
          }
          case L'n':
          {
            currentChar = L'\n';
            break;
          }
          case L'r':
          {
            currentChar = L'\r';
            break;
          }
          case L't':
          {
            currentChar = L'\t';
            break;
          }
          case L'u':
          {
            //We need 4 digits after the "u"
#pragma warning(suppress: 26486)
            if (wcslen(pszCurrentChar) < 5)
              throw CParseException(PARSE_EXCEPTION_REASON::ENCODED_UNICODE_CHARACTER_TOO_SHORT, pszCurrentChar - pszJSONPointer);

            //Parse out the 4 digits after the "u"
            currentChar = 0;
            for (int i=0; i<4; i++)
            {
#pragma warning(suppress: 26481)
              ++pszCurrentChar;
              currentChar <<= 4;
#pragma warning(suppress: 26489)
              if ((*pszCurrentChar >= '0') && (*pszCurrentChar <= '9'))
#pragma warning(suppress: 26489)
                currentChar |= (*pszCurrentChar - '0');
#pragma warning(suppress: 26489)
              else if ((*pszCurrentChar >= 'A') && (*pszCurrentChar <= 'F'))
                currentChar |= (10 + (*pszCurrentChar - 'A'));
#pragma warning(suppress: 26489)
              else if ((*pszCurrentChar >= 'a') && (*pszCurrentChar <= 'f'))
                currentChar |= (10 + (*pszCurrentChar - 'a'));
              else
                throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_ENCODED_UNICODE_CHARACTER, pszCurrentChar - pszJSONPointer);
            }
            break;
          }
          default:
          {
            throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_STRING, pszCurrentChar - pszJSONPointer);
            break;
          }
        }
      }
      else if (currentChar == L'/') //The start of the next token
      {
        DecodeJSONPointerToken(sReturn);
        return sReturn;
      }
      else if (currentChar < L' ') //A character which is not allowed
      {
        throw CParseException(PARSE_EXCEPTION_REASON::UNEXPECTED_CHARACTER_IN_STRING, pszCurrentChar - pszJSONPointer);
      }

      sReturn += currentChar;
#pragma warning(suppress: 26481 26487)
      ++pszCurrentChar;
    }

    DecodeJSONPointerToken(sReturn);
    return sReturn;
  }

  static void DecodeJSONPointerToken(_Inout_ std::wstring& sToken)
  {
    auto nFind = sToken.find(L"~1");
    while (nFind != std::wstring::npos)
    {
      sToken.replace(nFind, 2, L"/");
      nFind = sToken.find(L"~1");
    }
    nFind = sToken.find(L"~0");
    while (nFind != std::wstring::npos)
    {
      sToken.replace(nFind, 2, L"~");
      nFind = sToken.find(L"~0");
    }
  }
};


}; //namespace JSONPP

#endif //#ifndef __JSONPP_H__
