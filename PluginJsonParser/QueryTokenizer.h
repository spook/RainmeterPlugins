#ifndef __QUERYTOKENIZER_H__
#define __QUERYTOKENIZER_H__

#include <Windows.h>
#include <string>

enum QueryTokenType
{
	ttArrayIndex,
	ttArrayMember,
	ttDot,
	ttMember,
	ttUnknown
};

class QueryToken
{
public:
	QueryToken();
	QueryTokenType tokenType;
	std::wstring TokenStr;
};

class QueryTokenizer
{
private:
	int FStartState;

	int Transition(int state, const WCHAR ch);
public:
	QueryTokenizer();
	void Reset();
	QueryToken Process(const WCHAR * & AString);
	static std::wstring TokenTypeToStr(QueryTokenType tokenType);
};

#endif