#include <sstream>
#include <iostream>
#include <iomanip>
#include "Measure.h"
#include "StringTools.h"
#include "QueryParser.h"

const std::wstring Measure::convertToString(JSONPP::CValue* current)
{
	if (current->IsBoolean())
	{
		return current->AsBoolean() ? std::wstring(L"True") : std::wstring(L"False");
	}
	else if (current->IsNull())
	{
		if (this->debug == 1)
		{
			RmLog(this->rm, LOG_NOTICE, L"Found null");
		}

		return L"null";
	}
	else if (current->IsString())
	{
		if (this->debug == 1)
		{
			RmLog(this->rm, LOG_NOTICE, L"Found string");
		}

		return current->AsString();
	}
	else if (current->IsNumber())
	{
		if (this->debug == 1)
		{
			RmLog(this->rm, LOG_NOTICE, L"Found number");
		}

		std::wstringstream stream;
		stream << std::fixed << std::setprecision(this->precision) << current->AsNumber();
		return stream.str();
	}
	else if (current->IsArray())
	{
		if (this->debug == 1)
		{
			RmLog(this->rm, LOG_NOTICE, L"Found array (do you miss array index?)");
		}

		return std::wstring(L"(JSON Array)");
	}
	else if (current->IsObject())
	{
		if (this->debug == 1)
		{
			RmLog(this->rm, LOG_NOTICE, L"Found object (do you miss member access?)");
		}

		return std::wstring(L"(JSON Object)");
	}
	else
	{
		RmLog(this->rm, LOG_WARNING, L"Found not recognized object (report along with JSON and query to plugin author)");

		return std::wstring(L"");
	}
}

std::wstring Measure::parse(const std::wstring& source, const std::wstring& query)
{
	if (query.size() == 0 || source.size() == 0)
	{
		if (debug == 1)
		{
			RmLog(rm, LOG_NOTICE, L"Source or query is empty, nothing to parse");
		}

		return L"";
	}

	try
	{
		auto parsedQuery = QueryParser::parseQuery(query);

		JSONPP::CValue parsedSource;
		parsedSource.Parse(source.c_str());
			
		JSONPP::CValue * current = &parsedSource;

		for (unsigned int i = 0; i < parsedQuery.size(); i++)
		{
			auto memberAccess = std::dynamic_pointer_cast<MemberAccessQueryPart>(parsedQuery[i]);
			if (memberAccess != nullptr)
			{
				// Member access

				// Check if current is object and if it has child with specific member name
				if (!current->IsObject() || !current->HasChild(memberAccess->getMemberName().c_str()))
				{
					RmLog(this->rm, LOG_WARNING, L"Query returned no results.");
					return L"";
				}

				JSONPP::CValue& child = current->Child(memberAccess->getMemberName().c_str());
				current = &child;

				continue;
			}
			
			auto arrayAccess = std::dynamic_pointer_cast<ArrayItemAccessQueryPart>(parsedQuery[i]);
			if (arrayAccess != nullptr)
			{
				// Array access

				// Check if current is object and if index does not exceed array boundaries

				if (!current->IsArray() || arrayAccess->getIndex() < 0 || arrayAccess->getIndex() >= current->AsArray().size())
				{
					RmLog(this->rm, LOG_WARNING, L"Query returned no results.");
					return L""; // Not found
				}

				JSONPP::CValue& child = current->AsArray()[arrayAccess->getIndex()];
				current = &child;

				continue;
			}

			throw std::exception("Internal error: invalid item in query");
		}

		// Return as string
		return convertToString(current);
	}
	catch(std::exception & e)
	{
		auto error = std::wstring(L"ERROR: ");
		error.append(StringTools::stringToWideString(e.what()));
		
		RmLog(this->rm, LOG_ERROR, error.c_str());

		return L"";
	}
}

void Measure::evalQuery(const std::wstring& source, const std::wstring& query)
{
	if (debug == 1)
	{
		RmLog(rm, LOG_NOTICE, L"Manual query performed");

		std::wstring sourceDebug = L"Source: ";
		sourceDebug += source;
		RmLog(rm, LOG_NOTICE, sourceDebug.c_str());

		std::wstring queryDebug = L"Query: ";
		queryDebug += query;
		RmLog(rm, LOG_NOTICE, queryDebug.c_str());
	}

	std::wstring result = parse(source, query);
	queryBuffer = result;
}

Measure::Measure(void * rm)
{
	this->rm = rm;
	numeric = 0.0;
}

double Measure::getNumeric()
{
	return numeric;
}

std::wstring & Measure::getResult()
{
	return result;
}

std::wstring & Measure::getSource()
{
	return source;
}

int Measure::getDebug()
{
	return debug;
}

void* Measure::getRm()
{
	return rm;
}

void Measure::reload(void* rm, double* maxValue)
{
	this->rm = rm;

	if (debug == 1)
	{
		RmLog(rm, LOG_NOTICE, L"Refreshed");
	}

	update();
}

bool Measure::updateParameters()
{
	bool changed = false;

	// Load debug

	auto debugStr = std::wstring(RmReadString(rm, L"Debug", L"0", FALSE));
	int newDebug = 0;
	try
	{
		newDebug = std::stoi(debugStr);
	}
	catch (std::exception&)
	{
		newDebug = 0;
	}

	if (newDebug != this->debug)
	{
		this->debug = newDebug;
		changed = true;
	}

	// Load precision

	auto precisionStr = std::wstring(RmReadString(rm, L"Precision", L"2", FALSE));
	int newPrecision = 2;
	try
	{
		newPrecision = std::stoi(precisionStr);
	}
	catch (std::exception&)
	{
		newPrecision = 2;
	}

	if (newPrecision != this->precision)
	{
		this->precision = newPrecision;
		changed = true;
	}

	// Load source

	const WCHAR* newSourceChar = RmReadString(rm, L"Source", L"", FALSE);

	if (lstrcmpW(newSourceChar, this->source.c_str()) != 0)
	{
		this->source = std::wstring(newSourceChar);
		changed = true;
	}

	// Load query

	const WCHAR* newQueryChar = RmReadString(rm, L"Query", L"", FALSE);

	if (lstrcmpW(newQueryChar, this->query.c_str()) != 0)
	{
		this->query = std::wstring(newQueryChar);
		changed = true;
	}		

	return changed;
}

void Measure::update()
{
	// If parameters didn't change, do nothing.
	if (!updateParameters())
		return;

	if (debug == 1)
	{
		RmLog(rm, LOG_NOTICE, L"Updating");

		std::wstring sourceDebug = L"Source: ";
		sourceDebug += this->source;
		RmLog(rm, LOG_NOTICE, sourceDebug.c_str());

		std::wstring queryDebug = L"Query: ";
		queryDebug += this->query;
		RmLog(rm, LOG_NOTICE, queryDebug.c_str());
	}

	// Cache query result
	result = parse(source, query);

	// Try to convert result to double value
	try
	{
		this->numeric = std::stod(result);
	}
	catch (std::exception&)
	{
		this->numeric = 0.0;
	}
}

const std::wstring& Measure::getQueryBuffer()
{
	return this->queryBuffer;
}
