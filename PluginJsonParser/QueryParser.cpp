#include <Windows.h>
#include <sstream>
#include "QueryParser.h"
#include "QueryTokenizer.h"

ParsingException::ParsingException(const char* const message)
	: std::exception(message)
{

}

BaseQueryPart::BaseQueryPart()
{

}

BaseQueryPart::~BaseQueryPart()
{

}

// MemberAccessQueryPart implementation ---------------------------------------

MemberAccessQueryPart::MemberAccessQueryPart(std::wstring memberName)
	: memberName(memberName)
{
	
}

const std::wstring& MemberAccessQueryPart::getMemberName()
{
	return memberName;
}

// ArrayItemAccessQueryPart implementation ------------------------------------

ArrayItemAccessQueryPart::ArrayItemAccessQueryPart(unsigned int index)
	: index(index)
{

}

const unsigned int ArrayItemAccessQueryPart::getIndex()
{
	return index;
}

// QueryParser implementation -------------------------------------------------

std::vector<std::shared_ptr<BaseQueryPart>> QueryParser::parseQuery(std::wstring query)
{
	std::vector<std::shared_ptr<BaseQueryPart>> result;

	QueryTokenizer tokenizer;
	const WCHAR* current = query.c_str();

	bool expectMember = true;

	while (*current != 0)
	{
		QueryToken token = tokenizer.Process(current);

		if (expectMember && token.tokenType != QueryTokenType::ttMember)
		{
			// Todo more descriptive error
			throw ParsingException("Expected member!");
		}

		switch (token.tokenType)
		{
		case QueryTokenType::ttMember:
			{
				std::wstring member = token.TokenStr;
				result.push_back(std::shared_ptr<BaseQueryPart>(new MemberAccessQueryPart(member)));
				expectMember = false;
				break;
			}
		case QueryTokenType::ttArrayMember:
			{
				std::wstring member = token.TokenStr.substr(2, token.TokenStr.size() - 4);
				result.push_back(std::shared_ptr<BaseQueryPart>(new MemberAccessQueryPart(member)));
				break;
			}
		case QueryTokenType::ttArrayIndex:			
			{
				int index = std::stoi(token.TokenStr.substr(1, token.TokenStr.size() - 2));
				result.push_back(std::shared_ptr<BaseQueryPart>(new ArrayItemAccessQueryPart(index)));
				break;
			}
		case QueryTokenType::ttDot:
			{
				expectMember = true;
				break;
			}
		case QueryTokenType::ttUnknown:
			{
				throw ParsingException("Invalid query syntax!");
			}
		}
	}

	if (expectMember)
	{
		throw ParsingException("Expected member!");
	}

	return result;
}
