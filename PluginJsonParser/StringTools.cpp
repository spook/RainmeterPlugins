#include "StringTools.h"

std::string StringTools::wideStringToString(std::wstring wideString)
{
	int sizeRequired = WideCharToMultiByte(CP_UTF8, 0, &wideString[0], (int)wideString.size(), NULL, 0, NULL, NULL);
	std::string result(sizeRequired, 0);
	WideCharToMultiByte(CP_UTF8, 0, &wideString[0], (int)wideString.size(), &result[0], sizeRequired, NULL, NULL);
	return result;
}

std::string StringTools::wideStringToString(const WCHAR* wideString)
{
	int sizeRequired = WideCharToMultiByte(CP_UTF8, 0, wideString, -1, NULL, 0, NULL, NULL);
	std::string result(sizeRequired, 0);
	WideCharToMultiByte(CP_UTF8, 0, wideString, -1, &result[0], sizeRequired, NULL, NULL);
	return result;
}

std::wstring StringTools::stringToWideString(std::string string)
{
	int sizeRequired = MultiByteToWideChar(CP_UTF8, 0, &string[0], (int)string.size(), NULL, 0);
	std::wstring result(sizeRequired, 0);
	MultiByteToWideChar(CP_UTF8, 0, &string[0], (int)string.size(), &result[0], sizeRequired);
	return result;
}

std::wstring StringTools::stringToWideString(const char* string)
{
	int sizeRequired = MultiByteToWideChar(CP_UTF8, 0, string, -1, NULL, 0);
	std::wstring result(sizeRequired, 0);
	MultiByteToWideChar(CP_UTF8, 0, string, -1, &result[0], sizeRequired);
	return result;
}
