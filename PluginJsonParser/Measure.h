#include <Windows.h>
#include <string>
#include "JSON++.h"
#include "../API/RainmeterAPI.h"

#pragma once
class Measure
{
private:
	void* rm;
	double numeric = 0.0;
	std::wstring source = L"";
	std::wstring query = L"";
	std::wstring result = L"";
	int precision = 2;
	int debug = 0;
	std::wstring queryBuffer;

#ifdef UNIT_TESTS
public:
#endif
	const std::wstring convertToString(JSONPP::CValue* current);
	std::wstring parse(const std::wstring& source, const std::wstring& query);
	bool updateParameters();

public:
	Measure(void * rm);
	double getNumeric();
	std::wstring & getResult();
	std::wstring & getSource();
	int getDebug();
	const std::wstring& getQueryBuffer();
	void* getRm();

	void reload(void* rm, double* maxValue);
	void update();
	void evalQuery(const std::wstring& source, const std::wstring& query);

};

