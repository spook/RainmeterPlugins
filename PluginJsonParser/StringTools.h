#pragma once

#include <string>
#include <windows.h>

class StringTools
{
public:
	static std::string wideStringToString(std::wstring wideString);
	static std::string wideStringToString(const WCHAR * wideString);
	static std::wstring stringToWideString(std::string string);
	static std::wstring stringToWideString(const char* string);
};

