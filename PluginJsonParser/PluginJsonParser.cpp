#include <Windows.h>
#include "../API/RainmeterAPI.h"
#include "Measure.h"
#include "StringTools.h"

// Note: GetString, ExecuteBang and an unnamed function for use as a section variable
// have been commented out. Uncomment any functions as needed.
// For more information, see the SDK docs: https://docs.rainmeter.net/developers/plugin/cpp/



PLUGIN_EXPORT void Initialize(void ** data, void * rm)
{
	Measure * measure = new Measure(rm);
	*data = measure;
}

PLUGIN_EXPORT void Reload(void * data, void * rm, double * maxValue)
{
	Measure * measure = (Measure *)data;

	measure->reload(rm, maxValue);	
}

PLUGIN_EXPORT double Update(void * data)
{
	Measure* measure = (Measure*)data;

	measure->update();

	return measure->getNumeric();
}

PLUGIN_EXPORT LPCWSTR GetString(void * data)
{
	Measure * measure = (Measure *)data;
	return measure->getResult().c_str();
}

PLUGIN_EXPORT LPCWSTR Query(void * data, const int argc, const WCHAR * argv[])
{
	Measure * measure = (Measure *)data;

	std::wstring query;
	std::wstring source;

	if (argc == 1)
	{
		query = std::wstring(argv[0]);
		source = measure->getSource();
	}
	else if (argc == 2)
	{
		query = std::wstring(argv[0]);
		source = std::wstring(argv[1]);
	}
	else
	{
		// Error, invalid argument list
		return L"Invalid arguments!";
	}

	measure->evalQuery(source, query);
	return measure->getQueryBuffer().c_str();
}


//PLUGIN_EXPORT void ExecuteBang(void* data, LPCWSTR args)
//{
//	Measure* measure = (Measure*)data;
//}

//PLUGIN_EXPORT LPCWSTR (void* data, const int argc, const WCHAR* argv[])
//{
//	Measure* measure = (Measure*)data;
//	return nullptr;
//}

PLUGIN_EXPORT void Finalize(void* data)
{
	Measure* measure = (Measure*)data;
	delete measure;
}
