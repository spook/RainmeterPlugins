#include "CppUnitTest.h"
#include "..\PluginJsonParser\Measure.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PluginJsonParserTest
{
	TEST_CLASS(MeasureTests)
	{
		TEST_METHOD(measureParseSimpleString)
		{
			// Arrange

			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(
				L"{\"member\" : \"value\"}",
				L"member"
			);

			// Assert

			Assert::IsTrue(result == L"value");
		}

		TEST_METHOD(measureParseSimpleNumber)
		{
			// Arrange

			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(
				L"{\"member\" : 12}",
				L"member"
			);

			// Assert

			Assert::IsTrue(result == L"12.00");
		}

		TEST_METHOD(measureParseSimpleBool)
		{
			// Arrange

			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(
				L"{\"member\" : true}",
				L"member"
			);

			// Assert

			Assert::IsTrue(result == L"True");
		}

		TEST_METHOD(measureParseNestedMember)
		{
			// Arrange

			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(
				L"{\"nested\" : {\"member\" : \"value\"}}",
				L"nested.member"
			);

			// Assert

			Assert::IsTrue(result == L"value");
		}

		TEST_METHOD(measureParseNestedMemberAccessByArray)
		{
			// Arrange

			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(
				L"{\"nested\" : {\"member\" : \"value\"}}",
				L"nested['member']"
			);

			// Assert

			Assert::IsTrue(result == L"value");
		}

		TEST_METHOD(measureParseArray)
		{
			// Arrange

			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(
				L"{ \"values\" : [1, 2, 3, 4]}",
				L"values[0]"
			);

			// Assert

			Assert::IsTrue(result == L"1.00");
		}

		TEST_METHOD(measureParseArrayOfObjects)
		{
			// Arrange

			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(
				L"{ \"values\" : [ { \"member\" : \"value\" } ]}",
				L"values[0].member"
			);

			// Assert

			Assert::IsTrue(result == L"value");
		}

		TEST_METHOD(measureParseArrayOfObjectsAccessByArray)
		{
			// Arrange

			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(
				L"{ \"values\" : [ { \"member\" : \"value\" } ]}",
				L"values[0]['member']"
			);

			// Assert

			Assert::IsTrue(result == L"value");
		}

		TEST_METHOD(measureLongJson)
		{
			// Arrange

			std::wstring json;
			json += L"{\"lat\":51.1,\"lon\":17.03,\"timezone\":\"Europe/Warsaw\",\"timezone_offset\":7200,\"current\":{\"dt\":1599658537,\"sunrise\":1599625094,\"sunset\":1599672021,\"temp\":26.55,\"feels_like\":23.44,\"pressure\":1020,\"humidity\":36,\"dew_point\":10.27,\"uvi\":4.59,\"clouds\":0,\"visibility\":10000,\"wind_speed\":4.6,\"wind_deg\":230,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}]},\"minutely\":[{\"dt\":1599658560,\"precipitation\":0},{\"dt\":1599658620,\"precipitation\":0},{\"dt\":1599658680,\"precipitation\":0},{\"dt\":1599658740,\"precipitation\":0},{\"dt\":1599658800,\"precipitation\":0},{\"dt\":1599658860,\"precipitation\":0},{\"dt\":1599658920,\"precipitation\":0},{\"dt\":1599658980,\"precipitation\":0},{\"dt\":1599659040,\"precipitation\":0},{\"dt\":1599659100,\"precipitation\":0},{\"dt\":1599659160,\"precipitation\":0},{\"dt\":1599659220,\"precipitation\":0},{\"dt\":1599659280,\"precipitation\":0},{\"dt\":1599659340,\"precipitation\":0},{\"dt\":1599659400,\"precipitation\":0},{\"dt\":1599659460,\"precipitation\":0},{\"dt\":1599659520,\"precipitation\":0},{\"dt\":1599659580,\"precipitation\":0},{\"dt\":1599659640,\"precipitation\":0},{\"dt\":1599659700,\"precipitation\":0},{\"dt\":1599659760,\"precipitation\":0},{\"dt\":1599659820,\"precipitation\":0},{\"dt\":1599659880,\"precipitation\":0},{\"dt\":1599659940,\"precipitation\":0},{\"dt\":1599660000,\"precipitation\":0},{\"dt\":1599660060,\"precipitation\":0},{\"dt\":1599660120,\"precipitation\":0},{\"dt\":1599660180,\"precipitation\":0},{\"dt\":1599660240,\"precipitation\":0},{\"dt\":1599660300,\"precipitation\":0},{\"dt\":1599660360,\"precipitation\":0},{\"dt\":1599660420,\"precipitation\":0},{\"dt\":1599660480,\"precipitation\":0},{\"dt\":1599660540,\"precipitation\":0},{\"dt\":1599660600,\"precipitation\":0},{\"dt\":1599660660,\"precipitation\":0},{\"dt\":1599660720,\"precipitation\":0},{\"dt\":1599660780,\"precipitation\":0},{\"dt\":1599660840,\"precipitation\":0},{\"dt\":1599660900,\"precipitation\":0},{\"dt\":1599660960,\"precipitation\":0},{\"dt\":1599661020,\"precipitation\":0},{\"dt\":1599661080,\"precipitation\":0},{\"dt\":1599661140,\"precipitation\":0},{\"dt\":1599661200,\"precipitation\":0},{\"dt\":1599661260,\"precipitation\":0},{\"dt\":1599661320,\"precipitation\":0},{\"dt\":1599661380,\"precipitation\":0},{\"dt\":1599661440,\"precipitation\":0},{\"dt\":1599661500,\"precipitation\":0},{\"dt\":1599661560,\"precipitation\":0},{\"dt\":1599661620,\"precipitation\":0},{\"dt\":1599661680,\"precipitation\":0},{\"dt\":1599661740,\"precipitation\":0},{\"dt\":1599661800,\"precipitation\":0},{\"dt\":1599661860,\"precipitation\":0},{\"dt\":1599661920,\"precipitation\":0},{\"dt\":1599661980,\"precipitation\":0},{\"dt\":1599662040,\"precipitation\":0},{\"dt\":1599662100,\"precipitation\":0},{\"dt\":1599662160,\"precipitation\":0}],\"hourly\":[{\"dt\":1599656400,\"temp\":26.55,\"feels_like\":23.16,\"pressure\":1020,\"humidity\":36,\"dew_point\":10.27,\"clouds\":0,\"visibility\":10000,\"wind_speed\":5.01,\"wind_deg\":225,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599660000,\"temp\":25.66,\"feels_like\":22.52,\"pressure\":1020,\"humidity\":42,\"dew_point\":11.8,\"clouds\":0,\"visibility\":10000,\"wind_speed\":5.27,\"wind_deg\":225,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599663600,\"temp\":24.76,\"feels_like\":22.28,\"pressure\":1018,\"humidity\":48,\"dew_point\":13.02,\"clouds\":0,\"visibility\":10000,\"wind_speed\":4.88,\"wind_deg\":224,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599667200,\"temp\":23.21,\"feels_like\":22.04,\"pressure\":1018,\"humidity\":58,\"dew_point\":14.5,\"clouds\":0,\"visibility\":10000,\"wind_speed\":3.72,\"wind_deg\":224,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599670800,\"temp\":20.66,\"feels_like\":19.53,\"pressure\":1017,\"humidity\":65,\"dew_point\":13.85,\"clouds\":0,\"visibility\":10000,\"wind_speed\":3.34,\"wind_deg\":234,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599674400,\"temp\":19.24,\"feels_like\":18.07,\"pressure\":1017,\"humidity\":67,\"dew_point\":13,\"clouds\":0,\"visibility\":10000,\"wind_speed\":2.99,\"wind_deg\":237,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"pop\":0},{\"dt\":1599678000,\"temp\":18.61,\"feels_like\":17.31,\"pressure\":1018,\"humidity\":68,\"dew_point\":12.77,\"clouds\":0,\"visibility\":10000,\"wind_speed\":3,\"wind_deg\":227,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"pop\":0},{\"dt\":1599681600,\"temp\":17.95,\"feels_like\":16.31,\"pressure\":1018,\"humidity\":71,\"dew_point\":12.63,\"c";
			json += L"louds\":0,\"visibility\":10000,\"wind_speed\":3.5,\"wind_deg\":218,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"pop\":0},{\"dt\":1599685200,\"temp\":17.53,\"feels_like\":15.69,\"pressure\":1017,\"humidity\":72,\"dew_point\":12.53,\"clouds\":0,\"visibility\":10000,\"wind_speed\":3.7,\"wind_deg\":228,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"pop\":0},{\"dt\":1599688800,\"temp\":17.17,\"feels_like\":15.44,\"pressure\":1017,\"humidity\":73,\"dew_point\":12.42,\"clouds\":0,\"visibility\":10000,\"wind_speed\":3.48,\"wind_deg\":237,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"pop\":0},{\"dt\":1599692400,\"temp\":16.86,\"feels_like\":15.22,\"pressure\":1017,\"humidity\":73,\"dew_point\":12.07,\"clouds\":0,\"visibility\":10000,\"wind_speed\":3.23,\"wind_deg\":247,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"pop\":0},{\"dt\":1599696000,\"temp\":16.68,\"feels_like\":15.01,\"pressure\":1017,\"humidity\":72,\"dew_point\":11.71,\"clouds\":0,\"visibility\":10000,\"wind_speed\":3.1,\"wind_deg\":257,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01n\"}],\"pop\":0},{\"dt\":1599699600,\"temp\":17.13,\"feels_like\":15.22,\"pressure\":1017,\"humidity\":70,\"dew_point\":11.8,\"clouds\":42,\"visibility\":10000,\"wind_speed\":3.45,\"wind_deg\":263,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"pop\":0},{\"dt\":1599703200,\"temp\":17.87,\"feels_like\":15.82,\"pressure\":1016,\"humidity\":70,\"dew_point\":12.43,\"clouds\":67,\"visibility\":10000,\"wind_speed\":3.95,\"wind_deg\":267,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599706800,\"temp\":18.04,\"feels_like\":16.27,\"pressure\":1016,\"humidity\":75,\"dew_point\":13.69,\"clouds\":78,\"visibility\":10000,\"wind_speed\":4.11,\"wind_deg\":277,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599710400,\"temp\":17.85,\"feels_like\":16.47,\"pressure\":1017,\"humidity\":82,\"dew_point\":14.9,\"clouds\":84,\"visibility\":10000,\"wind_speed\":4.14,\"wind_deg\":283,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"pop\":0.1},{\"dt\":1599714000,\"temp\":17.32,\"feels_like\":15.64,\"pressure\":1018,\"humidity\":91,\"dew_point\":15.94,\"clouds\":87,\"visibility\":10000,\"wind_speed\":5.15,\"wind_deg\":291,\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"pop\":0.34,\"rain\":{\"1h\":0.56}},{\"dt\":1599717600,\"temp\":16.03,\"feels_like\":11.79,\"pressure\":1019,\"humidity\":83,\"dew_point\":13.17,\"clouds\":89,\"visibility\":10000,\"wind_speed\":7.46,\"wind_deg\":334,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0.45},{\"dt\":1599721200,\"temp\":15.64,\"feels_like\":11.61,\"pressure\":1020,\"humidity\":75,\"dew_point\":11.34,\"clouds\":100,\"visibility\":10000,\"wind_speed\":6.31,\"wind_deg\":326,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0.24},{\"dt\":1599724800,\"temp\":16.08,\"feels_like\":11.98,\"pressure\":1020,\"humidity\":68,\"dew_point\":10.29,\"clouds\":98,\"visibility\":10000,\"wind_speed\":5.99,\"wind_deg\":321,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0.16},{\"dt\":1599728400,\"temp\":16.89,\"feels_like\":12.99,\"pressure\":1021,\"humidity\":62,\"dew_point\":9.75,\"clouds\":89,\"visibility\":10000,\"wind_speed\":5.47,\"wind_deg\":317,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0.12},{\"dt\":1599732000,\"temp\":17.99,\"feels_like\":14.48,\"pressure\":1021,\"humidity\":57,\"dew_point\":9.57,\"clouds\":73,\"visibility\":10000,\"wind_speed\":4.83,\"wind_deg\":316,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"pop\":0.12},{\"dt\":1599735600,\"temp\":18.75,\"feels_like\":15.44,\"pressure\":1020,\"humidity\":51,\"dew_point\":8.5,\"clouds\":69,\"visibility\":10000,\"wind_speed\":4.2,\"wind_deg\":316,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"pop\":0.07},{\"dt\":1599739200,\"temp\":19.37,\"feels_like\":16.2,\"pressure\":1021,\"humidity\":47,\"dew_point\":7.86,\"clouds\":72,\"visibility\":10000,\"wind_speed\":3.79,\"wind_deg\":307,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"pop\":0.07},{\"dt\":1599742800,\"temp\":19.71,\"feels_like\":16.53,\"pressure\":1020,\"humidity\":45,\"dew_point\":7.65,\"clouds\":99,\"visibility\":10000,\"wind_spee";
			json += L"d\":3.69,\"wind_deg\":304,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0},{\"dt\":1599746400,\"temp\":19.65,\"feels_like\":16.74,\"pressure\":1020,\"humidity\":46,\"dew_point\":7.76,\"clouds\":99,\"visibility\":10000,\"wind_speed\":3.39,\"wind_deg\":303,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0},{\"dt\":1599750000,\"temp\":19.26,\"feels_like\":16.78,\"pressure\":1020,\"humidity\":47,\"dew_point\":7.89,\"clouds\":99,\"visibility\":10000,\"wind_speed\":2.76,\"wind_deg\":306,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0},{\"dt\":1599753600,\"temp\":18.44,\"feels_like\":17.06,\"pressure\":1020,\"humidity\":55,\"dew_point\":9.34,\"clouds\":99,\"visibility\":10000,\"wind_speed\":1.74,\"wind_deg\":317,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0},{\"dt\":1599757200,\"temp\":16.04,\"feels_like\":14.73,\"pressure\":1020,\"humidity\":60,\"dew_point\":8.51,\"clouds\":100,\"visibility\":10000,\"wind_speed\":1.3,\"wind_deg\":341,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"pop\":0},{\"dt\":1599760800,\"temp\":14.77,\"feels_like\":13.39,\"pressure\":1020,\"humidity\":63,\"dew_point\":7.98,\"clouds\":100,\"visibility\":10000,\"wind_speed\":1.23,\"wind_deg\":15,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599764400,\"temp\":13.98,\"feels_like\":12.6,\"pressure\":1021,\"humidity\":67,\"dew_point\":7.97,\"clouds\":100,\"visibility\":10000,\"wind_speed\":1.29,\"wind_deg\":34,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599768000,\"temp\":13.38,\"feels_like\":12.01,\"pressure\":1021,\"humidity\":69,\"dew_point\":7.87,\"clouds\":100,\"visibility\":10000,\"wind_speed\":1.23,\"wind_deg\":46,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599771600,\"temp\":12.89,\"feels_like\":11.64,\"pressure\":1021,\"humidity\":71,\"dew_point\":7.84,\"clouds\":100,\"visibility\":10000,\"wind_speed\":1.04,\"wind_deg\":74,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599775200,\"temp\":12.48,\"feels_like\":11.22,\"pressure\":1021,\"humidity\":73,\"dew_point\":7.79,\"clouds\":100,\"visibility\":10000,\"wind_speed\":1.06,\"wind_deg\":98,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599778800,\"temp\":11.9,\"feels_like\":10.65,\"pressure\":1020,\"humidity\":75,\"dew_point\":7.79,\"clouds\":92,\"visibility\":10000,\"wind_speed\":0.99,\"wind_deg\":120,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599782400,\"temp\":11.41,\"feels_like\":10.2,\"pressure\":1020,\"humidity\":78,\"dew_point\":7.74,\"clouds\":78,\"visibility\":10000,\"wind_speed\":0.97,\"wind_deg\":121,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599786000,\"temp\":11.12,\"feels_like\":9.88,\"pressure\":1020,\"humidity\":79,\"dew_point\":7.72,\"clouds\":65,\"visibility\":10000,\"wind_speed\":0.98,\"wind_deg\":131,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599789600,\"temp\":10.78,\"feels_like\":9.41,\"pressure\":1020,\"humidity\":81,\"dew_point\":7.71,\"clouds\":51,\"visibility\":10000,\"wind_speed\":1.18,\"wind_deg\":126,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04n\"}],\"pop\":0},{\"dt\":1599793200,\"temp\":10.5,\"feels_like\":8.94,\"pressure\":1020,\"humidity\":82,\"dew_point\":7.68,\"clouds\":39,\"visibility\":10000,\"wind_speed\":1.41,\"wind_deg\":141,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"pop\":0},{\"dt\":1599796800,\"temp\":10.26,\"feels_like\":8.68,\"pressure\":1019,\"humidity\":83,\"dew_point\":7.61,\"clouds\":44,\"visibility\":10000,\"wind_speed\":1.43,\"wind_deg\":151,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"pop\":0},{\"dt\":1599800400,\"temp\":10.52,\"feels_like\":9.39,\"pressure\":1019,\"humidity\":83,\"dew_point\":7.82,\"clouds\":45,\"visibility\":10000,\"wind_speed\":0.87,\"wind_deg\":133,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}],\"pop\":0},{\"dt\":1599804000,\"temp\":12.53,\"feels_like\":11.52,\"pressure\":1019,\"humidity\":75,\"dew_point\":8.38,\"clouds\":38,\"visibility\":10000,\"wind_speed\":0.86,\"wind_deg\":107,\"weather\":[{\"id\":802,";
			json += L"\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}],\"pop\":0},{\"dt\":1599807600,\"temp\":14.56,\"feels_like\":13.43,\"pressure\":1019,\"humidity\":67,\"dew_point\":8.72,\"clouds\":0,\"visibility\":10000,\"wind_speed\":1.13,\"wind_deg\":120,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599811200,\"temp\":16.29,\"feels_like\":14.76,\"pressure\":1020,\"humidity\":62,\"dew_point\":9.07,\"clouds\":0,\"visibility\":10000,\"wind_speed\":1.87,\"wind_deg\":138,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599814800,\"temp\":17.79,\"feels_like\":15.88,\"pressure\":1019,\"humidity\":57,\"dew_point\":9.31,\"clouds\":0,\"visibility\":10000,\"wind_speed\":2.47,\"wind_deg\":153,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599818400,\"temp\":19,\"feels_like\":17.11,\"pressure\":1019,\"humidity\":54,\"dew_point\":9.5,\"clouds\":0,\"visibility\":10000,\"wind_speed\":2.57,\"wind_deg\":164,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599822000,\"temp\":20.05,\"feels_like\":18.29,\"pressure\":1019,\"humidity\":50,\"dew_point\":9.43,\"clouds\":0,\"visibility\":10000,\"wind_speed\":2.32,\"wind_deg\":171,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0},{\"dt\":1599825600,\"temp\":21.1,\"feels_like\":19.96,\"pressure\":1018,\"humidity\":48,\"dew_point\":9.74,\"clouds\":0,\"visibility\":10000,\"wind_speed\":1.56,\"wind_deg\":178,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"pop\":0}],\"daily\":[{\"dt\":1599645600,\"sunrise\":1599625094,\"sunset\":1599672021,\"temp\":{\"day\":26.55,\"min\":18.11,\"max\":26.55,\"night\":18.11,\"eve\":25.44,\"morn\":26.55},\"feels_like\":{\"day\":23.37,\"night\":16.31,\"eve\":22.73,\"morn\":23.37},\"pressure\":1020,\"humidity\":36,\"dew_point\":10.27,\"wind_speed\":4.71,\"wind_deg\":223,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"clouds\":0,\"pop\":0,\"uvi\":4.59},{\"dt\":1599732000,\"sunrise\":1599711587,\"sunset\":1599758285,\"temp\":{\"day\":16.89,\"min\":12.89,\"max\":19.37,\"night\":12.89,\"eve\":19.26,\"morn\":18.04},\"feels_like\":{\"day\":12.99,\"night\":11.64,\"eve\":16.78,\"morn\":16.27},\"pressure\":1021,\"humidity\":62,\"dew_point\":9.75,\"wind_speed\":5.47,\"wind_deg\":317,\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"clouds\":89,\"pop\":0.45,\"rain\":0.75,\"uvi\":4.27},{\"dt\":1599818400,\"sunrise\":1599798081,\"sunset\":1599844549,\"temp\":{\"day\":17.79,\"min\":10.5,\"max\":21.6,\"night\":15.07,\"eve\":21.6,\"morn\":10.5},\"feels_like\":{\"day\":15.88,\"night\":13.32,\"eve\":20.98,\"morn\":8.94},\"pressure\":1019,\"humidity\":57,\"dew_point\":9.31,\"wind_speed\":2.47,\"wind_deg\":153,\"weather\":[{\"id\":800,\"main\":\"Clear\",\"description\":\"clear sky\",\"icon\":\"01d\"}],\"clouds\":0,\"pop\":0,\"uvi\":4.17},{\"dt\":1599904800,\"sunrise\":1599884574,\"sunset\":1599930812,\"temp\":{\"day\":21.99,\"min\":13.54,\"max\":26.77,\"night\":19.04,\"eve\":26.77,\"morn\":13.54},\"feels_like\":{\"day\":20.9,\"night\":18.43,\"eve\":26.16,\"morn\":11.65},\"pressure\":1017,\"humidity\":60,\"dew_point\":13.97,\"wind_speed\":3.29,\"wind_deg\":149,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"clouds\":96,\"pop\":0,\"uvi\":4.2},{\"dt\":1599991200,\"sunrise\":1599971067,\"sunset\":1600017075,\"temp\":{\"day\":21.24,\"min\":16.03,\"max\":23.22,\"night\":16.03,\"eve\":22.91,\"morn\":17.88},\"feels_like\":{\"day\":19.93,\"night\":15.65,\"eve\":21.31,\"morn\":17.68},\"pressure\":1022,\"humidity\":62,\"dew_point\":13.88,\"wind_speed\":3.51,\"wind_deg\":310,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"clouds\":95,\"pop\":0.02,\"uvi\":4.2},{\"dt\":1600077600,\"sunrise\":1600057560,\"sunset\":1600103338,\"temp\":{\"day\":20.75,\"min\":13.98,\"max\":24.06,\"night\":16.92,\"eve\":24.06,\"morn\":13.98},\"feels_like\":{\"day\":20.06,\"night\":14.84,\"eve\":23.09,\"morn\":14.12},\"pressure\":1027,\"humidity\":58,\"dew_point\":12.29,\"wind_speed\":1.95,\"wind_deg\":126,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"clouds\":80,\"pop\":0,\"uvi\":4.13},{\"dt\":1600164000,\"sunrise\":1600144053,\"sunset\":1600189600,\"temp\":{\"day\":23.74,\"min\":14.72,\"max\":28.55,\"night\":20.39,\"eve\":28.55,\"morn\":14.72},\"feels_like\":{\"day\":23.04,\"night\":19.68,\"eve\":29.07,\"morn\":12.64},\"pressure\":1020,\"humidity\":61,\"dew_point\":15.81,\"wind_speed\":3.71,\"wind_deg\":151,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds";
			json += L"\",\"icon\":\"04d\"}],\"clouds\":87,\"pop\":0,\"uvi\":4.28},{\"dt\":1600250400,\"sunrise\":1600230546,\"sunset\":1600275863,\"temp\":{\"day\":26.19,\"min\":17.83,\"max\":28.9,\"night\":19.78,\"eve\":27.07,\"morn\":17.83},\"feels_like\":{\"day\":24.24,\"night\":18.25,\"eve\":25.79,\"morn\":16.71},\"pressure\":1012,\"humidity\":49,\"dew_point\":14.69,\"wind_speed\":4.9,\"wind_deg\":162,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}],\"clouds\":46,\"pop\":0,\"uvi\":4.08}]}";
			Measure measure(nullptr);

			// Act

			std::wstring result = measure.parse(json, L"current.temp");

			// Assert

			Assert::IsTrue(result == L"26.55");
		}


	};
}