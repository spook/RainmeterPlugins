#include "CppUnitTest.h"
#include "..\PluginJsonParser\QueryParser.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PluginJsonParserTest
{
	TEST_CLASS(QueryParserTests)
	{
		TEST_METHOD(queryParserSingle)
		{
			// Act

			auto result = QueryParser::parseQuery(L"member");

			// Assert

			Assert::IsTrue(result.size() == 1);

			std::shared_ptr<MemberAccessQueryPart> memberAccess = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[0]);
			Assert::IsTrue(memberAccess != nullptr);
			Assert::IsTrue(memberAccess->getMemberName() == L"member");
		}

		TEST_METHOD(queryParserTwoMembers)
		{
			// Act

			auto result = QueryParser::parseQuery(L"member1.member2");

			// Assert

			Assert::IsTrue(result.size() == 2);

			std::shared_ptr<MemberAccessQueryPart> member1Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[0]);
			Assert::IsTrue(member1Access != nullptr);
			Assert::IsTrue(member1Access->getMemberName() == L"member1");

			std::shared_ptr<MemberAccessQueryPart> member2Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[1]);
			Assert::IsTrue(member2Access != nullptr);
			Assert::IsTrue(member2Access->getMemberName() == L"member2");
		}

		TEST_METHOD(queryParserTwoMembersByArrayQuotes)
		{
			// Act

			auto result = QueryParser::parseQuery(L"member1[\"member2\"]");

			// Assert

			Assert::IsTrue(result.size() == 2);

			std::shared_ptr<MemberAccessQueryPart> member1Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[0]);
			Assert::IsTrue(member1Access != nullptr);
			Assert::IsTrue(member1Access->getMemberName() == L"member1");

			std::shared_ptr<MemberAccessQueryPart> member2Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[1]);
			Assert::IsTrue(member2Access != nullptr);
			Assert::IsTrue(member2Access->getMemberName() == L"member2");
		}

		TEST_METHOD(queryParserTwoMembersByArrayApostrophe)
		{
			// Act

			auto result = QueryParser::parseQuery(L"member1[\'member2\']");

			// Assert

			Assert::IsTrue(result.size() == 2);

			std::shared_ptr<MemberAccessQueryPart> member1Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[0]);
			Assert::IsTrue(member1Access != nullptr);
			Assert::IsTrue(member1Access->getMemberName() == L"member1");

			std::shared_ptr<MemberAccessQueryPart> member2Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[1]);
			Assert::IsTrue(member2Access != nullptr);
			Assert::IsTrue(member2Access->getMemberName() == L"member2");
		}

		TEST_METHOD(queryParserArrayAccess)
		{
			// Act

			auto result = QueryParser::parseQuery(L"member1[4]");

			// Assert

			Assert::IsTrue(result.size() == 2);

			std::shared_ptr<MemberAccessQueryPart> member1Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[0]);
			Assert::IsTrue(member1Access != nullptr);
			Assert::IsTrue(member1Access->getMemberName() == L"member1");

			std::shared_ptr<ArrayItemAccessQueryPart> member2Access = std::dynamic_pointer_cast<ArrayItemAccessQueryPart>(result[1]);
			Assert::IsTrue(member2Access != nullptr);
			Assert::IsTrue(member2Access->getIndex() == 4);
		}

		TEST_METHOD(queryParserArrayAccessByParenthesis)
		{
			// Act

			auto result = QueryParser::parseQuery(L"member1(4)");

			// Assert

			Assert::IsTrue(result.size() == 2);

			std::shared_ptr<MemberAccessQueryPart> member1Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[0]);
			Assert::IsTrue(member1Access != nullptr);
			Assert::IsTrue(member1Access->getMemberName() == L"member1");

			std::shared_ptr<ArrayItemAccessQueryPart> member2Access = std::dynamic_pointer_cast<ArrayItemAccessQueryPart>(result[1]);
			Assert::IsTrue(member2Access != nullptr);
			Assert::IsTrue(member2Access->getIndex() == 4);
		}

		TEST_METHOD(queryParserTwoMembersWithSpaceByArrayQuotes)
		{
			// Act

			auto result = QueryParser::parseQuery(L"member1[\"member2 with space\"]");

			// Assert

			Assert::IsTrue(result.size() == 2);

			std::shared_ptr<MemberAccessQueryPart> member1Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[0]);
			Assert::IsTrue(member1Access != nullptr);
			Assert::IsTrue(member1Access->getMemberName() == L"member1");

			std::shared_ptr<MemberAccessQueryPart> member2Access = std::dynamic_pointer_cast<MemberAccessQueryPart>(result[1]);
			Assert::IsTrue(member2Access != nullptr);
			Assert::IsTrue(member2Access->getMemberName() == L"member2 with space");
		}

		TEST_METHOD(queryParserThrowOnInvalidCharacter)
		{
			// Act & Assert

			Assert::ExpectException<ParsingException, void>([]()
				{
					auto result = QueryParser::parseQuery(L"member1>member2");
				});			
		}
	};
}