#include "CppUnitTest.h"
#include "..\PluginJsonParser\QueryTokenizer.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PluginJsonParserTest
{
	TEST_CLASS(PluginJsonParserTests)
	{
	private:
		void verifySingleMember(std::wstring input, QueryTokenType expectedToken)
		{
			// Arrange

			QueryTokenizer tokenizer;
			const WCHAR* queryCh = input.c_str();

			// Act
			auto token = tokenizer.Process(queryCh);

			// Assert

			Assert::IsTrue(expectedToken == token.tokenType);
		}

	public:
		
		TEST_METHOD(queryTokenizerRecognizesMember)
		{
			verifySingleMember(L"member", QueryTokenType::ttMember);
		}

		TEST_METHOD(queryTokenizerRecognizesDot)
		{
			verifySingleMember(L".", QueryTokenType::ttDot);
		}

		TEST_METHOD(queryTokenizerRecognizesArrayMemberWithQuotes)
		{
			verifySingleMember(L"[\"member\"]", QueryTokenType::ttArrayMember);
		}

		TEST_METHOD(queryTokenizerRecognizesArrayMemberWithApostrophe)
		{
			verifySingleMember(L"['member']", QueryTokenType::ttArrayMember);
		}

		TEST_METHOD(queryTokenizerRecognizesArrayIndex)
		{
			verifySingleMember(L"[44]", QueryTokenType::ttArrayIndex);
		}

		TEST_METHOD(queryTokenizerRecognizesSubsequentParts)
		{
			// Arrange

			std::wstring input = L"a[\"b\"].c[8]";
			QueryTokenizer tokenizer;
			const WCHAR* queryCh = input.c_str();

			// Act & Assert

			QueryToken token;

			token = tokenizer.Process(queryCh);
			Assert::IsTrue(QueryTokenType::ttMember == token.tokenType);
			token = tokenizer.Process(queryCh);
			Assert::IsTrue(QueryTokenType::ttArrayMember == token.tokenType);
			token = tokenizer.Process(queryCh);
			Assert::IsTrue(QueryTokenType::ttDot == token.tokenType);
			token = tokenizer.Process(queryCh);
			Assert::IsTrue(QueryTokenType::ttMember == token.tokenType);
			token = tokenizer.Process(queryCh);
			Assert::IsTrue(QueryTokenType::ttArrayIndex == token.tokenType);

			Assert::IsTrue(*queryCh == 0);
		}
	};
}
