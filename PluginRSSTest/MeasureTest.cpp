#include "CppUnitTest.h"

#include "..\PluginRSS\BaseMeasureApi.h"
#include "..\PluginRSS\BaseDownloader.h"
#include "..\PluginRSS\BaseLogger.h"
#include "..\PluginRSS\InternetDownloader.h"
#include "..\PluginRSS\Measure.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace MeasureTest
{
	class MeasureApiMock : public BaseMeasureApi
	{
		std::wstring readString(std::wstring optionName, std::wstring defaultValue)
		{
			if (optionName == L"Url")
			{
				return L"https://link/one";
			}
			else if (optionName == L"Url2")
			{
				return L"https://link/two";
			}
			else
				return defaultValue;
		}

		int readInt(std::wstring optionName, int defaultValue)
		{
			if (optionName == L"PageSize")
				return 2;

            return defaultValue;
		}
	};

    class RealUrlMeasureApiMock : public BaseMeasureApi
    {
        std::wstring readString(std::wstring optionName, std::wstring defaultValue)
        {
            if (optionName == L"Url")
            {
                return L"https://www.nintendoswitch.pl/feed";
            }
            else
                return defaultValue;
        }

        int readInt(std::wstring optionName, int defaultValue)
        {
            if (optionName == L"PageSize")
                return 2;

            return defaultValue;
        }
    };

	class DownloaderMock : public BaseDownloader
	{
		std::string download(std::wstring url)
		{
            if (url == L"https://link/one")
            {
                return "<rss>\
  <channel>\
    <title>Channel1</title>\
    <item>\
      <title>Ch1Item1Title</title>\
      <link>Ch1Item1Link</link>\
      <description>Ch1Item1Desc</description>\
    </item>\
    <item>\
      <title>Ch1Item2Title</title>\
      <link>Ch1Item2Link</link>\
      <description>Ch1Item2Desc</description>\
    </item>\
    <item>\
      <title>Ch1Item3Title</title>\
      <link>Ch1Item3Link</link>\
      <description>Ch1Item3Desc</description>\
    </item>\
      <item>\
      <title>Ch1Item4Title</title>\
      <link>Ch1Item4Link</link>\
      <description>Ch1Item4Desc</description>\
    </item>\
    <item>\
      <title>Ch1Item5Title</title>\
      <link>Ch1Item5Link</link>\
      <description>Ch1Item5Desc</description>\
    </item>\
  </channel>\
</rss>";
            }
            else if (url == L"https://link/two")
            {
                return "<rss>\
  <channel>\
    <title>Channel2</title>\
    <item>\
      <title>Ch1Item2Title</title>\
      <link>Ch1Item2Link</link>\
      <description>Ch2Item1Desc</description>\
    </item>\
    <item>\
      <title>Ch2Item2Title</title>\
      <link>Ch2Item2Link</link>\
      <description>Ch2Item2Desc</description>\
    </item>\
    <item>\
      <title>Ch2Item3Title</title>\
      <link>Ch2Item3Link</link>\
      <description>Ch2Item3Desc</description>\
    </item>\
      <item>\
      <title>Ch2Item4Title</title>\
      <link>Ch2Item4Link</link>\
      <description>Ch2Item4Desc</description>\
    </item>\
    <item>\
      <title>Ch2Item5Title</title>\
      <link>Ch2Item5Link</link>\
      <description>Ch2Item5Desc</description>\
    </item>\
  </channel>\
</rss>";
            }
            else
                return "";
		}
	};

    class LoggerMock : public BaseLogger
    {
        void log(std::wstring message)
        {

        }
    };

	TEST_CLASS(MeasureTest)
	{
	public:
		
		TEST_METHOD(MeasureLoadsXmlFromDownloader)
		{
            // Arrange
            Measure measure(std::shared_ptr<BaseMeasureApi>(new MeasureApiMock()),
                std::shared_ptr<BaseDownloader>(new DownloaderMock()),
                std::shared_ptr<BaseLogger>(new LoggerMock()));

            // Act
            measure.update();

            // Assert
            Assert::AreEqual(measure.getChannelCount(), 2);
		}

        TEST_METHOD(MeasureSwitchesToNextChannel)
        {
            // Arrange
            Measure measure(std::shared_ptr<BaseMeasureApi>(new MeasureApiMock()),
                std::shared_ptr<BaseDownloader>(new DownloaderMock()),
                std::shared_ptr<BaseLogger>(new LoggerMock()));

            // Act
            measure.update();

            // Assert
            Assert::AreEqual(std::wstring(L"Channel1"), measure.getCurrentChannelTitle());
            measure.nextChannel();
            Assert::AreEqual(std::wstring(L"Channel2"), measure.getCurrentChannelTitle());
            measure.nextChannel();
            Assert::AreEqual(std::wstring(L"Channel2"), measure.getCurrentChannelTitle());
        }

        TEST_METHOD(MeasureSwitchesToPreviousChannel)
        {
            // Arrange
            Measure measure(std::shared_ptr<BaseMeasureApi>(new MeasureApiMock()),
                std::shared_ptr<BaseDownloader>(new DownloaderMock()),
                std::shared_ptr<BaseLogger>(new LoggerMock()));

            // Act
            measure.update();

            // Assert
            Assert::AreEqual(std::wstring(L"Channel1"), measure.getCurrentChannelTitle());
            measure.previousChannel();
            Assert::AreEqual(std::wstring(L"Channel1"), measure.getCurrentChannelTitle());
            measure.nextChannel();
            Assert::AreEqual(std::wstring(L"Channel2"), measure.getCurrentChannelTitle());
            measure.previousChannel();
            Assert::AreEqual(std::wstring(L"Channel1"), measure.getCurrentChannelTitle());
        }

        TEST_METHOD(MeasureSwitchesToNextPage)
        {
            // Arrange
            Measure measure(std::shared_ptr<BaseMeasureApi>(new MeasureApiMock()),
                std::shared_ptr<BaseDownloader>(new DownloaderMock()),
                std::shared_ptr<BaseLogger>(new LoggerMock()));

            // Act
            measure.update();

            // Assert
            Assert::AreEqual(std::wstring(L"Ch1Item1Title"), measure.getEntryTitle(0));
            measure.nextPage();
            Assert::AreEqual(std::wstring(L"Ch1Item3Title"), measure.getEntryTitle(0));
        }

        TEST_METHOD(MeasureSwitchesToPreviousPage)
        {
            // Arrange
            Measure measure(std::shared_ptr<BaseMeasureApi>(new MeasureApiMock()),
                std::shared_ptr<BaseDownloader>(new DownloaderMock()),
                std::shared_ptr<BaseLogger>(new LoggerMock()));

            // Act
            measure.update();

            // Assert
            Assert::AreEqual(std::wstring(L"Ch1Item1Title"), measure.getEntryTitle(0));
            measure.nextPage();
            Assert::AreEqual(std::wstring(L"Ch1Item3Title"), measure.getEntryTitle(0));
            measure.previousPage();
            Assert::AreEqual(std::wstring(L"Ch1Item1Title"), measure.getEntryTitle(0));
        }

        TEST_METHOD(MeasureReadsItemData)
        {
            // Arrange
            Measure measure(std::shared_ptr<BaseMeasureApi>(new MeasureApiMock()),
                std::shared_ptr<BaseDownloader>(new DownloaderMock()),
                std::shared_ptr<BaseLogger>(new LoggerMock()));

            // Act
            measure.update();

            // Assert
            Assert::AreEqual(std::wstring(L"Ch1Item1Title"), measure.getEntryTitle(0));
            Assert::AreEqual(std::wstring(L"Ch1Item1Desc"), measure.getEntryDescription(0));
            Assert::AreEqual(std::wstring(L"Ch1Item1Link"), measure.getEntryLink(0));
        }

        TEST_METHOD(MeasureReadsDataFromInternet) 
        {
            // Arrange
            Measure measure(std::shared_ptr<BaseMeasureApi>(new RealUrlMeasureApiMock()),
                std::shared_ptr<BaseDownloader>(new InternetDownloader()),
                std::shared_ptr<BaseLogger>(new LoggerMock()));

            // Act
            measure.update();

            // Assert
            Assert::AreEqual(1, measure.getChannelCount());
            Assert::IsTrue(measure.getCurrentChannelEntryCount() > 0);
        }
    };
}
