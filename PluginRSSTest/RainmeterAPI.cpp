#include <Windows.h>
#include "..\API\RainmeterAPI.h"

LPCWSTR __stdcall RmReadString(void* rm, LPCWSTR option, LPCWSTR defValue, BOOL replaceMeasures)
{
	return L"";
}

double __stdcall RmReadFormula(void* rm, LPCWSTR option, double defValue)
{
	return 0.0;
}

LPCWSTR __stdcall RmReplaceVariables(void* rm, LPCWSTR str)
{
	return L"";
}

LPCWSTR __stdcall RmPathToAbsolute(void* rm, LPCWSTR relativePath)
{
	return L"";
}

void __stdcall RmExecute(void* skin, LPCWSTR command)
{

}

void* __stdcall RmGet(void* rm, int type)
{
	return nullptr;
}

void __stdcall RmLog(void* rm, int level, LPCWSTR message)
{

}

void __cdecl RmLogF(void* rm, int level, LPCWSTR format, ...)
{

}

BOOL __cdecl LSLog(int level, LPCWSTR unused, LPCWSTR message)
{
	return FALSE;
}
