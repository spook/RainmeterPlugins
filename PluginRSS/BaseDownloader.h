#pragma once

#include <string>

class BaseDownloader
{
public:
	virtual std::string download(std::wstring url) = 0;
};

