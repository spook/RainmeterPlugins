#pragma once

#include <Windows.h>
#include "..\API\RainmeterAPI.h"
#include "BaseLogger.h"

class RainmeterLogger : public BaseLogger
{
private:
	void* rm;

public:
	RainmeterLogger(void* rm);
	void log(std::wstring message);
};

