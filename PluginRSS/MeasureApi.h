#pragma once

#include <Windows.h>
#include <string>
#include <functional>

#include "BaseMeasureApi.h"

class MeasureApi : public BaseMeasureApi
{
private:
	void* rm;

public:
	MeasureApi(void* rm);

	std::wstring readString(std::wstring optionName, std::wstring defaultValue);
	int readInt(std::wstring optionName, int defaultValue);
};
