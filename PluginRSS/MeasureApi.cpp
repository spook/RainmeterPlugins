#include "MeasureApi.h"
#include "../API/RainmeterAPI.h"

MeasureApi::MeasureApi(void* rm)
{
	this->rm = rm;
}

std::wstring MeasureApi::readString(std::wstring optionName, std::wstring defaultValue)
{
	return std::wstring(RmReadString(rm, optionName.c_str(), defaultValue.c_str()));
}

int MeasureApi::readInt(std::wstring optionName, int defaultValue)
{
	return RmReadInt(rm, optionName.c_str(), defaultValue);
}
