#pragma once

#include <string>
#include <vector>
#include <memory>

#include "Entry.h"

class Channel
{
private:
	const std::wstring title;
	std::vector<std::shared_ptr<Entry>> entries;

public:
	Channel(const std::wstring title, std::vector<std::shared_ptr<Entry>> entries);

	std::vector<std::shared_ptr<Entry>> getEntries();
	const std::wstring getTitle();
};
