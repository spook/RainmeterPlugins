#include "InternetDownloader.h"

#include <Windows.h>
#include <iostream>
#include <Wininet.h>
#include <string>
#include <xlocbuf>

std::string InternetDownloader::download(const std::wstring url)
{
	HINTERNET interwebs = InternetOpenW(L"Mozilla/5.0", INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, NULL);
	HINTERNET urlFile;
	std::string result;

	if (interwebs)
	{
		urlFile = InternetOpenUrlW(interwebs, url.c_str(), NULL, NULL, NULL, NULL);
		if (urlFile)
		{
			char buffer[2000];
			DWORD bytesRead;
			do
			{
				InternetReadFile(urlFile, buffer, 2000, &bytesRead);
				result.append(buffer, bytesRead);
			} while (bytesRead);

			InternetCloseHandle(urlFile);
		}

		InternetCloseHandle(interwebs);
	}

	return result;
}