#include "Channel.h"

Channel::Channel(const std::wstring title, std::vector<std::shared_ptr<Entry>> entries)
	: title(title), entries(entries)
{

}

std::vector<std::shared_ptr<Entry>> Channel::getEntries()
{
	return this->entries;
}

const std::wstring Channel::getTitle()
{
	return this->title;
}