#pragma once

#include <string>

class BaseMeasureApi
{
public:
	virtual std::wstring readString(std::wstring optionName, std::wstring defaultValue) = 0;
	virtual int readInt(std::wstring optionName, int defaultValue) = 0;
};

