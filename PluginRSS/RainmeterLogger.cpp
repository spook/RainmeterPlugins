#include "RainmeterLogger.h"

RainmeterLogger::RainmeterLogger(void* rm)
{
	this->rm = rm;
}

void RainmeterLogger::log(std::wstring message)
{
	RmLog(this->rm, LOG_NOTICE, message.c_str());
}
