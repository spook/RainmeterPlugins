#pragma once

#include <memory>
#include <string>
#include "lib\pugixml\pugixml.hpp"
#include "Channel.h"
#include "BaseMeasureApi.h"
#include "BaseDownloader.h"
#include "BaseLogger.h"

struct Measure
{
private:
	std::shared_ptr<BaseMeasureApi> measureApi;
	std::shared_ptr<BaseDownloader> downloader;
	std::shared_ptr<BaseLogger> logger;
	std::vector<std::shared_ptr<Channel>> channels;
	int pageSize;
	int currentChannel;
	int currentPage;
	bool debugMode;

	std::wstring resultBuffer;

	int getMaxPageFor(int channel);
	void sanitizeCurrentPosition();
	std::shared_ptr<Channel> buildSource(std::shared_ptr<pugi::xml_document> doc);
	std::vector<std::wstring> getUrlsFromOptions();
	std::shared_ptr<Entry> getCurrentEntry(int offset);

public:
	Measure(std::shared_ptr<BaseMeasureApi> measureApi, std::shared_ptr<BaseDownloader> downloader, std::shared_ptr<BaseLogger> logger);
	void update();

	void reloadSources();

	int getChannelCount();
	int getCurrentChannelEntryCount();

	std::wstring getCurrentChannelTitle();
	std::wstring getEntryTitle(int offset);
	std::wstring getEntryDescription(int offset);
	std::wstring getEntryLink(int offset);

	void currentChannelTitleToResultBuffer();
	void entryTitleToResultBuffer(int offset);
	void entryDescriptionToResultBuffer(int offset);
	void entryLinkToResultBuffer(int offset);
	void pageInfoToResultBuffer();

	const wchar_t* getResultBuffer();

	void nextPage();
	void previousPage();
	void nextChannel();
	void previousChannel();
};
