#pragma once

#include <string>

class Entry
{
private:
	const std::wstring title;
	const std::wstring description;
	const std::wstring link;

public:
	Entry(const std::wstring header, const std::wstring contents, const std::wstring link);		

	const std::wstring getTitle();
	const std::wstring getDescription();
	const std::wstring getLink();
};
