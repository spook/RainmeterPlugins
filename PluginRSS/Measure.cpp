#define NOMINMAX

#include <algorithm>
#include <string>
#include <sstream>
#include <vector>
#include <memory>

#include "Measure.h"
#include "MeasureApi.h"

Measure::Measure(std::shared_ptr<BaseMeasureApi> measureApi, 
	std::shared_ptr<BaseDownloader> downloader,
	std::shared_ptr<BaseLogger> logger)
	: measureApi(measureApi), downloader(downloader), logger(logger)
{
	this->pageSize = 1;
	this->currentPage = 0;
	this->currentChannel = 0;
}

int Measure::getMaxPageFor(int channel)
{
	if (channel < 0 || channel >= channels.size())
		return 0;

	int channelSize = (int)channels[channel]->getEntries().size();

	int maxPage = channelSize / pageSize;
	if (channelSize % pageSize == 0)
		maxPage--;

	return maxPage;
}

void Measure::sanitizeCurrentPosition() 
{
	currentChannel = std::max(0, std::min(currentChannel, (int)channels.size() - 1));
	if (channels.size() > 0) 
	{
		int maxPage = getMaxPageFor(currentChannel);
		currentPage = std::max(0, std::min(currentPage, maxPage));
	}
	else
	{
		currentPage = 0;
	}
}

std::shared_ptr<Channel> Measure::buildSource(std::shared_ptr<pugi::xml_document> doc)
{
	std::vector<std::shared_ptr<Entry>> entries;

	std::wstring channelName;
	auto titleNode = doc->select_node(L"/rss/channel[1]/title[1]");
	if (titleNode)
		channelName = titleNode.node().first_child().value();

	auto items = doc->select_nodes(L"/rss/channel[1]/item");
	for (auto& item : items)
	{
		std::wstring itemTitle;
		auto itemTitleNode = item.node().select_node(L"title");
		if (itemTitleNode)
			itemTitle = itemTitleNode.node().first_child().value();

		std::wstring itemDesc;
		auto itemDescNode = item.node().select_node(L"description");
		if (itemDescNode)
			itemDesc = itemDescNode.node().first_child().value();

		std::wstring itemLink;
		auto itemLinkNode = item.node().select_node(L"link");
		if (itemLinkNode)
			itemLink = itemLinkNode.node().first_child().value();

		std::shared_ptr<Entry> entry(new Entry(itemTitle, itemDesc, itemLink));
		entries.push_back(entry);
	}

	return std::shared_ptr<Channel>(new Channel(channelName, entries));
}

void Measure::update()
{
	this->pageSize = measureApi->readInt(L"PageSize", 1);
	this->debugMode = measureApi->readInt(L"Debug", 0) == 1;

	reloadSources();	
}

std::vector<std::wstring> Measure::getUrlsFromOptions()
{
	std::vector<std::wstring> urls;

	int i = 1;
	bool urlFound;
	do
	{
		std::wstring urlName = L"Url";
		if (i > 1)
		{
			urlName.append(std::to_wstring(i));
		}

		auto url = measureApi->readString(urlName, L"");
		urlFound = url.length() > 0;
		if (urlFound)
			urls.push_back(url);

		i++;
	} while (urlFound);

	return urls;
}

std::shared_ptr<Entry> Measure::getCurrentEntry(int offset)
{
	if (currentChannel < 0 || currentChannel >= channels.size())
		return std::shared_ptr<Entry>(nullptr);

	int index = currentPage * pageSize + offset;

	if (index < 0 || index >= channels[currentChannel]->getEntries().size())
		return std::shared_ptr<Entry>(nullptr);

	return channels[currentChannel]->getEntries()[index];
}

void Measure::reloadSources() 
{
	std::vector<std::wstring> urls = getUrlsFromOptions();

	channels.clear();

	for (auto &url : urls) 
	{
		std::string data = downloader->download(url);

		std::shared_ptr<pugi::xml_document> doc(new pugi::xml_document());

		auto result = doc->load_buffer(data.c_str(), data.length());
		if (result.status == pugi::xml_parse_status::status_ok) 
		{
			std::shared_ptr<Channel> channel = buildSource(doc);
			channels.push_back(channel);
		}
	}

	sanitizeCurrentPosition();
}

int Measure::getChannelCount()
{
	return (int)channels.size();
}

int Measure::getCurrentChannelEntryCount()
{
	if (channels.size() == 0)
		return 0;
	else
		return (int)channels[currentChannel]->getEntries().size();
}

std::wstring Measure::getCurrentChannelTitle()
{
	if (channels.size() == 0)
		return L"";
	return channels[currentChannel]->getTitle();
}

std::wstring Measure::getEntryTitle(int offset)
{
	auto entry = getCurrentEntry(offset);
	if (entry.get() != nullptr)
		return entry->getTitle();
	else
		return L"";
}

std::wstring Measure::getEntryDescription(int offset)
{
	auto entry = getCurrentEntry(offset);
	if (entry.get() != nullptr)
		return entry->getDescription();
	else
		return L"";
}

std::wstring Measure::getEntryLink(int offset)
{
	auto entry = getCurrentEntry(offset);
	if (entry.get() != nullptr)
		return entry->getLink();
	else
		return L"";
}

void Measure::currentChannelTitleToResultBuffer()
{
	resultBuffer = getCurrentChannelTitle();
}

void Measure::entryTitleToResultBuffer(int offset)
{
	resultBuffer = getEntryTitle(offset);
}

void Measure::entryDescriptionToResultBuffer(int offset)
{
	resultBuffer = getEntryDescription(offset);
}

void Measure::entryLinkToResultBuffer(int offset)
{
	resultBuffer = getEntryLink(offset);
}

void Measure::pageInfoToResultBuffer()
{
	std::wstringstream str;
	str << (currentPage + 1) << " / " << (getMaxPageFor(currentChannel) + 1);

	resultBuffer = str.str();
}

const wchar_t* Measure::getResultBuffer() 
{
	return resultBuffer.c_str();
}

void Measure::nextPage()
{
	int maxPage = getMaxPageFor(currentChannel);
	currentPage = std::min(maxPage, currentPage + 1);
}

void Measure::previousPage()
{
	currentPage = std::max(0, currentPage - 1);
}

void Measure::nextChannel()
{
	int maxChannel = std::max(0, (int)channels.size() - 1);
	int nextCurrentChannel = std::min(maxChannel, currentChannel + 1);
	if (currentChannel != nextCurrentChannel)
	{
		currentChannel = nextCurrentChannel;
		currentPage = 0;
	}
}

void Measure::previousChannel()
{
	currentChannel = std::max(0, currentChannel - 1);
}
