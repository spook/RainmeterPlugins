#include "Entry.h"

Entry::Entry(const std::wstring title, const std::wstring description, const std::wstring link)
	: title(title), description(description), link(link)
{

}

const std::wstring Entry::getTitle() 
{
	return title;
}

const std::wstring Entry::getDescription()
{
	return description;
}

const std::wstring Entry::getLink()
{
	return link;
}
