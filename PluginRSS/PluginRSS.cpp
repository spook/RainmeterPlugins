#include <Windows.h>
#include "Measure.h"
#include "BaseMeasureApi.h"
#include "BaseDownloader.h"
#include "MeasureApi.h"
#include "InternetDownloader.h"
#include "RainmeterLogger.h"
#include "../API/RainmeterAPI.h"

// Overview: This is a blank canvas on which to build your plugin.

// Note: GetString, ExecuteBang and an unnamed function for use as a section variable
// have been commented out. Uncomment any functions as needed.
// For more information, see the SDK docs: https://docs.rainmeter.net/developers/plugin/cpp/

/*std::function<std::wstring(std::wstring, std::wstring)> apiReadString,
std::function<int(std::wstring, int)> apiReadInt)*/

PLUGIN_EXPORT void Initialize(void** data, void* rm)
{
	auto measureApi = std::shared_ptr<BaseMeasureApi>(new MeasureApi(rm));
	auto downloader = std::shared_ptr<BaseDownloader>(new InternetDownloader());
	auto logger = std::shared_ptr<BaseLogger>(new RainmeterLogger(rm));

	Measure* measure = new Measure(measureApi, downloader, logger);
	*data = measure;
}

PLUGIN_EXPORT void Reload(void* data, void* rm, double* maxValue)
{
	Measure* measure = (Measure*)data;
}

PLUGIN_EXPORT double Update(void* data)
{
	Measure* measure = (Measure*)data;
	measure->update();
	return 0.0;
}

//PLUGIN_EXPORT LPCWSTR GetString(void* data)
//{
//	Measure* measure = (Measure*)data;
//	return L"";
//}

PLUGIN_EXPORT void ExecuteBang(void* data, LPCWSTR args)
{
	Measure* measure = (Measure*)data;

	if (wcscmp(args, L"PreviousChannel") == 0)
	{
		measure->previousChannel();
	}
	else if (wcscmp(args, L"PreviousPage") == 0)
	{
		measure->previousPage();
	}
	else if (wcscmp(args, L"NextPage") == 0)
	{
		measure->nextPage();
	}
	else if (wcscmp(args, L"NextChannel") == 0)
	{
		measure->nextChannel();
	}
}

//PLUGIN_EXPORT LPCWSTR (void* data, const int argc, const WCHAR* argv[])
//{
//	Measure* measure = (Measure*)data;
//	return nullptr;
//}

PLUGIN_EXPORT LPCWSTR GetCurrentChannelTitle(void* data, const int argc, const WCHAR* argv[])
{
	Measure* measure = (Measure*)data;
	measure->currentChannelTitleToResultBuffer();
	return measure->getResultBuffer();
}

PLUGIN_EXPORT LPCWSTR GetEntryTitle (void* data, const int argc, const WCHAR* argv[])
{
	Measure* measure = (Measure*)data;
	int offset = 0;
	if (argc >= 1)
		offset = _wtoi(argv[0]);

	measure->entryTitleToResultBuffer(offset);
	return measure->getResultBuffer();
}

PLUGIN_EXPORT LPCWSTR GetEntryDescription(void* data, const int argc, const WCHAR* argv[])
{
	Measure* measure = (Measure*)data;
	int offset = 0;
	if (argc >= 1)
		offset = _wtoi(argv[0]);

	measure->entryDescriptionToResultBuffer(offset);
	return measure->getResultBuffer();
}

PLUGIN_EXPORT LPCWSTR GetEntryLink(void* data, const int argc, const WCHAR* argv[])
{
	Measure* measure = (Measure*)data;
	int offset = 0;
	if (argc >= 1)
		offset = _wtoi(argv[0]);

	measure->entryLinkToResultBuffer(offset);
	return measure->getResultBuffer();
}

PLUGIN_EXPORT LPCWSTR GetPageInfo(void* data, const int argc, const WCHAR* argv[])
{
	Measure* measure = (Measure*)data;
	
	measure->pageInfoToResultBuffer();
	return measure->getResultBuffer();
}

PLUGIN_EXPORT void Finalize(void* data)
{
	Measure* measure = (Measure*)data;
	delete measure;
}
