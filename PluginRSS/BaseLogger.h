#pragma once

#include <string>

class BaseLogger
{
public:
	virtual void log(std::wstring message) = 0;
};