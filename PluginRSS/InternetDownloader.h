#pragma once

#include "BaseDownloader.h"

class InternetDownloader : public BaseDownloader
{
	std::string download(std::wstring url);
};

